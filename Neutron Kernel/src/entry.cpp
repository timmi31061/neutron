#include <neutron.h>

#include "multiboot.h"

mb_struct* mbinfo;

//#pragma comment(linker, "/merge:.z=.text")
#pragma data_seg(push)
#pragma data_seg(".z")
__declspec(align(4)) __declspec(deprecated) void* endOfImage;
#pragma data_seg(pop)

extern "C" void kmain(uint32_t magic, mb_struct* mbinfo);

__declspec(naked) void kentry() {
	_asm {
		// Adjust stack
		mov esp, KERNEL_STACK;

		// Store multiboot-info
		mov mbinfo, ebx;

		// Push multiboot-info
		push ebx; // Multiboot-Struct
		push eax; // Multiboot-Magic

		// Invoke kmain
		call kmain;

		// Halt the processor without disabling interrupts.
	kentry_halt:
		hlt;
		jmp kentry_halt;
	}
}

MULTIBOOT_HEADER(LOAD_BASE, kentry);

