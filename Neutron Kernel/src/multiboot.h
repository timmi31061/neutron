#pragma once
#include <neutron.h>

#define MB_HEADER_MAGIC 0x1BADB002
#define MB_MAGIC 0x2BADB002
#define MB_FLAGS 0x10003
#define MB_CHECKSUM -(MB_HEADER_MAGIC + MB_FLAGS)

struct MULTIBOOT_HEADER {
	uint32_t magic;
	uint32_t flags;
	uint32_t checksum;
	uint32_t header_addr;
	uint32_t load_addr;
	uint32_t load_end_addr;
	uint32_t bss_end_addr;
	uint32_t entry_addr;
};

#define MULTIBOOT_HEADER(load_base, kernel_entry)		\
	__pragma(warning(push))								\
	__pragma(warning(disable: 4254))					\
	__pragma(comment(linker, "/merge:.a=.text"))		\
	__pragma(data_seg(push))							\
	__pragma(data_seg(".a"))							\
	__declspec(align(4)) MULTIBOOT_HEADER mb_header {	\
		(uint32_t)MB_HEADER_MAGIC,						\
		(uint32_t)MB_FLAGS,								\
		(uint32_t)MB_CHECKSUM,							\
		(uint32_t)(&mb_header),							\
		(uint32_t)(load_base),							\
		0,												\
		0,												\
		(uint32_t)(&(kernel_entry))						\
	};													\
	__pragma(data_seg(pop))								\
	__pragma(warning(pop))

extern MULTIBOOT_HEADER mb_header;

struct mb_module {
	uint32_t startAddress;
	uint32_t endAddress;
	char* name;
	uint32_t _reserved;
};

struct mb_mmap {
	uint32_t structSize;
	uint64_t address;
	uint64_t length;
	uint32_t type;
};

struct mb_drive {
	uint32_t structSize;
	uint8_t driveNumber;
	uint8_t driveMode;
	uint16_t driveCylinders;
	uint8_t driveHeads;
	uint8_t driveSectorsPerTrack;
};

struct mb_struct {
	uint32_t flags;
	uint32_t memLower;
	uint32_t memUpper;
	uint32_t bootDevice;
	char* cmdLine;

	uint32_t moduleCount;
	mb_module* modules;

	uint64_t _reserved0;
	uint64_t _reserved1;

	uint32_t mmapLength;
	mb_mmap* mmap;

	uint32_t drivesLength;
	mb_drive* drives;
};

extern mb_struct* mbinfo;
