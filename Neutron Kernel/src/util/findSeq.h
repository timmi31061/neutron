#pragma once
#include <neutron.h>

/**
 * Finds a bit sequence in the given buffer.
 *
 * @param	buf		uint8_t*	The buffer to search in.
 * @param	bufLen	uint32_t	The length of the buffer, in bytes.
 * @param	len		uint32_t	The count of bits to search for
 * @return	uint32_t	0, if nothing was found, otherwise, the bit number (0-based).
 */
uint32_t NEUTRON_MODULE_API findSeq(uint8_t* buf, uint32_t bufLen, uint32_t len);
