#pragma once
#include <neutron.h>

void NEUTRON_MODULE_API dumpMem(uint32_t start, size_t len);
void NEUTRON_MODULE_API dumpMem(uintptr_t ptr, size_t len);
