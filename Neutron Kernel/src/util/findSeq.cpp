#include <neutron.h>
#include <util/findSeq.h>

uint32_t NEUTRON_MODULE_API findSeq(uint8_t* buf, uint32_t bufLen, uint32_t len) {
	uint32_t startbit = 0;
	uint32_t stopbit = 0;

	// decrement len for 0-based calculations
	len -= 1;

	// try to find bit sequence
	for (uint32_t byte = 0; byte < bufLen; byte++) {
		for (uint32_t bit = 0; bit < 8; bit++) {
			uint8_t bitmask = (1 << bit);
			if (startbit) {
				if (buf[byte] & bitmask) {
					stopbit++;

					// check len
					if (stopbit - startbit >= len) {
						return startbit;
					}
				}
				else {
					startbit = 0;
				}
			}
			else {
				if (buf[byte] & bitmask) {
					stopbit = startbit = byte * 8 + bit;
				}
			}
		}
	}

	// no sequence found
	return 0;
}
