#include <neutron.h>
#include <kiostream.h>
#include <klibc\itoa.h>
#include <klibc\string.h>

void NEUTRON_MODULE_API dumpMem(uint32_t start, size_t len) {
	//char* itoaBuf = "  ";
	//start -= start % 16;

	/*for (uint32_t i = 0; i < len; i++) {
		if (i % 16 == 0) {
			kprintf("\n%X: ", start + i);
		}

		uint32_t byte = reinterpret_cast<uint8_t*>(start)[i];

		char* defaultBuf = "00";
		_memcpy(itoaBuf, defaultBuf, 5);
		itoa(itoaBuf, byte, 16, true, true, 1);
		kout << itoaBuf << " ";

		if ((start + i) % 8 == 7 && (start + i) % 16 != 0) {
			kout << " ";
		}
	}*/

	char* defaultBuf = "00";
	char* itoaBuf = "  ";

	uint8_t* bytes = reinterpret_cast<uint8_t*>(start);

	for (uint32_t i = 0; i < len; i += 16) {
		kprintf("%X: ", i);

		for (uint32_t j = 0; j < 16; j++) {
			if (j % 2 == 0) {
				kout << ' ';
			}

			_memcpy(itoaBuf, defaultBuf, 2);
			itoa(itoaBuf, bytes[i + j], 16, true, true, 1);
			kout << itoaBuf;

			if (j == 7) {
				kout << ' ';
				kout << ' ';
			}
		}

		kout << "    ";

		for (uint32_t j = 0; j < 16; j++) {
			char c = static_cast<char>(bytes[i + j]);

			if (c == '\r' || c == '\n' || c == '\t') {
				c = 0;
			}

			kout << c;
		}

		kout << '\n';
	}
}

void NEUTRON_MODULE_API dumpMem(uintptr_t ptr, size_t len) {
	dumpMem(reinterpret_cast<uint32_t>(ptr), len);
}