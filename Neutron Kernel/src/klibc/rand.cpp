#include <neutron.h>

uint32_t seed = 282564481;

extern "C" {
	NEUTRON_MODULE_API double rand() {
		uint32_t next = seed;
		uint32_t result;

		next *= 1103515245;
		next += 12345;
		result = (next / 65536) % 2048;

		next *= 1103515245;
		next += 12345;
		result <<= 10;
		result ^= (next / 65536) % 1024;

		next *= 1103515245;
		next += 12345;
		result <<= 10;
		result ^= (next / 65536) % 1024;

		seed = next;

		return result / 4294967295.0;
	}
}
