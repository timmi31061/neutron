#include <neutron.h>
#include <kiostream.h>
#include "string.h"

#pragma warning(disable: 4706)

extern "C" {
#pragma function(strlen)
	NEUTRON_MODULE_API size_t strlen(const char* str) {
		uint32_t size = 0;

		while (str[size++] != 0);

		return static_cast<size_t>(size - 1);
	}

#pragma function(strcpy)
	NEUTRON_MODULE_API char* strcpy(char* dst, const char* src) {
		char* _dst = dst;
		while (*dst++ = *src++);
		return _dst;
	}

#pragma function(strcat)
	NEUTRON_MODULE_API char* strcat(char* dst, const char* src) {
		char* _dst = dst;
		while (*dst) dst++;
		strcpy(dst, src);
		return _dst;
	}

#pragma function(strcmp)
	NEUTRON_MODULE_API int strcmp(const char* s1, const char* s2) {

		bool flag = false;
		while (*s1 && *s2 && (flag = (*s1++ == *s2++)));

		if (*s1 != 0 || *s2 != 0) {
			return 1;
		}

		return flag ? 0 : 1;
	}

#pragma function(memcpy)
	NEUTRON_MODULE_API uintptr_t memcpy(uintptr_t dst, const uintptr_t src, size_t len) {
		char* d = (char*)dst;
		char* s = (char*)src;

		for (uint32_t i = 0; i < len; i++) {
			d[i] = s[i];
		}

		return dst;
	}

#pragma function(memset)
	NEUTRON_MODULE_API uintptr_t memset(uintptr_t dst, uint8_t val, size_t len) {
		uint8_t* d = (uint8_t*)dst;

		for (uint32_t i = 0; i < len; i++) {
			d[i] = val;
		}

		return dst;
	}

	NEUTRON_MODULE_API uintptr_t memsetw(uintptr_t dst, uint16_t val, size_t len) {
		uint8_t* d = (uint8_t*)dst;

		for (uint32_t i = 0; i < len; i++) {
			d[i++] = (uint8_t)val;

			if (i < len) {
				d[i] = (uint8_t)(val >> 8);
			}
		}

		return dst;
	}
}

NEUTRON_MODULE_API uintptr_t _memcpy(uintptr_t dst, const uintptr_t src, size_t len) {
	return memcpy(dst, src, len);
}
