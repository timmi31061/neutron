#pragma once
#include <neutron.h>

extern "C" {
	NEUTRON_MODULE_API size_t strlen(const char* str);
//#pragma intrinsic(strlen)

	NEUTRON_MODULE_API char* strcpy(char* dst, const char* src);
	NEUTRON_MODULE_API char* strcat(char* dst, const char* src);
	NEUTRON_MODULE_API int strcmp(const char* s1, const char* s2);

	NEUTRON_MODULE_API uintptr_t  memcpy(uintptr_t dst, const uintptr_t src, size_t len);
//#pragma intrinsic(memcpy)

	NEUTRON_MODULE_API uintptr_t memset(uintptr_t dst, uint8_t val, size_t len);
//#pragma intrinsic(memset)

	NEUTRON_MODULE_API uintptr_t memsetw(uintptr_t dst, uint16_t val, size_t len);
}

NEUTRON_MODULE_API uintptr_t _memcpy(uintptr_t dst, const uintptr_t src, size_t len);