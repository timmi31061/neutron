#pragma once
#include <neutron.h>

extern "C" {
	NEUTRON_MODULE_API void kprintf(const char* format, ...);
	NEUTRON_MODULE_API void kvprintf(const char* format, va_list args);
	NEUTRON_MODULE_API char* ksprintf(char* destination, const char* format, ...);
	NEUTRON_MODULE_API char* kvsprintf(char* destination, const char* format, va_list args);
}
