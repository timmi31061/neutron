#pragma once
#include <neutron.h>

extern "C" NEUTRON_MODULE_API uint32_t crc32(uintptr_t data, size_t length);
