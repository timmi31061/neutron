#pragma once
#include <neutron.h>

extern "C" {
	NEUTRON_MODULE_API uint8_t itoa(char* buf, uint32_t num, uint8_t base = 10, bool terminate = true, bool rtl = false, uint8_t start = 0);
}
