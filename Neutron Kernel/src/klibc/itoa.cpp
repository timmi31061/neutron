#include <neutron.h>

#include "itoa.h"

extern "C" {
	NEUTRON_MODULE_API uint8_t itoa(char* buf, uint32_t num, uint8_t base, bool terminate, bool rtl, uint8_t start) {
		char* chars = "0123456789ABCDEF";
		char b[21];

		uint8_t digit = 0;
		while (num) {
			b[digit++] = chars[num % base];
			num /= base;
		}

		if (rtl) {
			for (int i = 0; i < digit; i++) {
				buf[start - i] = b[i];
			}
		}
		else {
			for (int i = 0; i < digit; i++) {
				buf[i + start] = b[digit - i - 1];
			}
		}

		if (digit == 0) {
			buf[start + digit++] = chars[0];
		}

		if (terminate) {
			buf[start + digit] = 0;
		}

		return digit;
	}
}
