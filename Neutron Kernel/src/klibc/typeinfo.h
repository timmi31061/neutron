#pragma once
#include <neutron.h>

#pragma warning(push)
#pragma warning(disable: 4200)
class type_info {
public:
	uint32_t	hash;	// Hash value computed from type's decorated name
	void* 	spare;		// reserved, possible for RTTI
	char	name[];		// The decorated name of the type; 0 terminated.
};
#pragma warning(pop)
