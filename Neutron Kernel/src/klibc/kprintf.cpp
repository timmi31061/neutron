#include <neutron.h>
#include <klibc/itoa.h>
#include <klibc/string.h>
#include <kiostream.h>

extern "C" {
	const char* hexDefaultBuf = "0x00000000";

	NEUTRON_MODULE_API void kprintf(const char* format, ...) {
		va_list args;
		va_start(args, format);

		kvprintf(format, args);

		va_end(args);
	}

	NEUTRON_MODULE_API void kvprintf(const char* format, va_list args) {
		bool cursorEnabled = kout.isCursorEnabled();
		kout.disableCursor(); // Disable cursor during write (moving it for every char is very slow)

		char* itoaBuf = "                    ";

		while (*format) {
			switch (*format)
			{
			case '%':
				format++;
				switch (*format) {
				case '%':
					kout << '%';
					break;

				case 's':
					kout << va_arg(args, char*);
					break;

				case 'c':
					kout << va_arg(args, char);
					break;

				case 'd':
					kout << va_arg(args, uint32_t);
					break;

				case 'x':
					itoa(itoaBuf, va_arg(args, uint32_t), 16);
					kout << itoaBuf;
					break;

				case 'X':
					strcpy(itoaBuf, hexDefaultBuf);
					itoa(itoaBuf, va_arg(args, uint32_t), 16, true, true, 9);
					kout << itoaBuf;
					break;
				}
				break;
			default:
				kout << *format;
				break;
			}
			format++;
		}

		if (cursorEnabled) {
			kout.enableCursor();
		}
	}

	static void ksprintf_strcpy(char* dst, const char* src, uint32_t& index) {
		while ((dst[index] = *(src++)) != 0) {
			index++;
		}
	}

	NEUTRON_MODULE_API char* ksprintf(char* destination, const char* format, ...) {
		va_list args;
		va_start(args, format);

		kvsprintf(destination, format, args);

		va_end(args);
		return destination;
	}

	NEUTRON_MODULE_API char* kvsprintf(char* destination, const char* format, va_list args) {
		uint32_t index = 0;
		char* volatile itoaBuf = "                    ";
		while (*format) {
			switch (*format) {
			case '%':
				format++;
				switch (*format) {
				case '%':
					destination[index++] = '%';
					break;

				case 's':
					ksprintf_strcpy(destination, va_arg(args, char*), index);
					break;

				case 'c':
					destination[index++] = va_arg(args, char);
					break;

				case 'd':
					itoa(itoaBuf, va_arg(args, uint32_t), 10);
					ksprintf_strcpy(destination, itoaBuf, index);
					break;

				case 'x':
					itoa(itoaBuf, va_arg(args, uint32_t), 16);
					ksprintf_strcpy(destination, itoaBuf, index);
					break;

				case 'X':
					strcpy(itoaBuf, hexDefaultBuf);
					itoa(itoaBuf, va_arg(args, uint32_t), 16, true, true, 9);
					ksprintf_strcpy(destination, itoaBuf, index);
					break;
				}
				break;
			default:
				destination[index++] = *format;
				break;
			}
			format++;
		}

		destination[index] = 0;

		return destination;
	}
}
