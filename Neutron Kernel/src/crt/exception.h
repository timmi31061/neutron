/**
 * exception.h
 * 
 * Contents of this file are written using research in ehdata.h (Visual C++ CRT, Copyright (c) 1993-1995, Microsoft Corporation).
 */

#pragma once
#include <neutron.h>

#pragma warning(push)
#pragma warning(disable: 4200)

typedef void(*PMFN)(void*);
typedef int32_t ptrdiff_t;
typedef int __ehstate_t;

typedef struct PMD {
	ptrdiff_t	mdisp;		// Offset of intended data within base
	ptrdiff_t	pdisp;		// Displacement to virtual base pointer
	ptrdiff_t	vdisp;		// Index within vbTable to offset of base
} PMD;

typedef struct TypeDescriptor {
	uint32_t	hash;	// Hash value computed from type's decorated name
	void* 	spare;		// reserved, possible for RTTI
	char	name[];		// The decorated name of the type; 0 terminated.
} TypeDescriptor;

typedef const struct _s_CatchableType {
	unsigned int	properties;			// Catchable Type properties (Bit field)
	TypeDescriptor* pType;				// Pointer to the type descriptor for this type
	PMD 			thisDisplacement;	// Pointer to instance of catch type within thrown object.
	int				sizeOrOffset;		// Size of simple-type object or offset into buffer of 'this' pointer for catch object
	PMFN			copyFunction;		// Copy constructor or CC-closure
} CatchableType;

typedef const struct _s_CatchableTypeArray {
	int	nCatchableTypes;
	CatchableType*	arrayOfCatchableTypes;
} CatchableTypeArray;

typedef const struct _s_ThrowInfo {
	unsigned int	attributes;			// Throw Info attributes (Bit field)
	PMFN			pmfnUnwind;			// Destructor to call when exception has been handled or aborted.

	int(__cdecl* pForwardCompat)(...);			// Forward compatibility frame handler
	CatchableTypeArray*	pCatchableTypeArray;	// Pointer to list of pointers to types.
} ThrowInfo;


typedef const struct _s_HandlerType {
	unsigned int	adjectives;			// Handler Type adjectives (bitfield)
	TypeDescriptor*	pType;				// Pointer to the corresponding type descriptor
	ptrdiff_t		dispCatchObj;		// Displacement of catch object from base
	void*			addressOfHandler;	// Address of 'catch' code
} HandlerType;

typedef const struct _s_UnwindMapEntry {
	__ehstate_t		toState;	// State this action takes us to
	void(*action)(void);		// Funclet to call to effect state change
} UnwindMapEntry;

typedef const struct _s_TryBlockMapEntry {
	__ehstate_t		tryLow;				// Lowest state index of try
	__ehstate_t		tryHigh;			// Highest state index of try
	__ehstate_t		catchHigh;			// Highest state index of any associated catch

	int				nCatches;			// Number of entries in array
	HandlerType*	pHandlerArray;		// List of handlers for this try
} TryBlockMapEntry;

typedef const struct _s_FuncInfo {
	uint32_t			magicNumber;	// Identifies version of compiler

	__ehstate_t			nUnwindCount;	// number of entries in unwind map
	UnwindMapEntry*		pUnwindMap;		// Where the unwind map is
	unsigned int		nTryBlocks;		// Number of 'try' blocks in this function
	TryBlockMapEntry*	pTryBlockMap;	// Where the handler map is
} FuncInfo;

typedef struct _s_ChainLink {
	//uint32_t ebp;
	_s_ChainLink*	prev;
	FuncInfo*	(*handler)();
	int	id;
} ChainLink;

#pragma warning(pop)
