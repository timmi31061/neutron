#include <neutron.h>
#include <kiostream.h>
#include <neutron/NeutronKernel.h>
#include <neutron/exceptions/SystemException.h>
#include <kstl/list.h>
#include "exception.h"

using namespace Neutron;
using namespace Neutron::Exceptions;

static void unhandledException(uint32_t returnAddress, SystemException* exception) {
	NeutronKernel::getInstance()->panic(returnAddress, "Unhandled exception: %s", exception->message());
}

extern "C" uintptr_t CxxThrowException(uint32_t returnAddress, ChainLink* link, SystemException** obj, ThrowInfo* info) {
	// EBP of handler
	uint32_t ebp = 1;

	// Save handler here
	HandlerType* c = 0;

	// Walk call stack
	while (ebp != 0) {
		// link is equal to caller's ebp
		ebp = reinterpret_cast<uint32_t>(link);		

		// link is pointing at the end of the struct. Fix that.
		link -= 1;

		// Is current code in a try?
		if (link->id == -1) {
			unhandledException(returnAddress, *obj);
		}

		// Calculate ID of 'try'
		int tryID = link->id / 2;

		// Check for sanity
		if (tryID > 65536) {
			// This function doesn't have a chain link struct on stack
			// Go one function up
			link = *reinterpret_cast<ChainLink**>(ebp); // link is now pointing at the end, but this will be corrected by the next iteration
			continue;
		}

		// Call handler to fetch function info
		FuncInfo* funcInfo = link->handler();

		// Does this 'try' exist?
		if (static_cast<uint32_t>(tryID) >= funcInfo->nTryBlocks) {
			NeutronKernel::getInstance()->panic(tryID, "CxxThrowException: Unknown try-block.");
		}

		// Try to find catch
		{
			uint32_t typeCount = info->pCatchableTypeArray->nCatchableTypes;
			CatchableType* types = (info->pCatchableTypeArray->arrayOfCatchableTypes);

			uint32_t catchCount = funcInfo->pTryBlockMap[tryID].nCatches;
			HandlerType* catches = funcInfo->pTryBlockMap[tryID].pHandlerArray;

			bool foundCatch = false;
			for (uint32_t catchID = 0; catchID < catchCount && !foundCatch; catchID++) {
				for (uint32_t typeID = 0; typeID < typeCount && !foundCatch; typeID++) {
					if (catches[catchID].pType == types[typeID].pType) {
						c = &catches[catchID];
						foundCatch = true;
					}
				}
			}

			if (foundCatch) {
				break;
			}
		}
		
		// Go one function up
		link = *reinterpret_cast<ChainLink**>(ebp); // link is now pointing at the end, but this will be corrected by the next iteration
	}

	if (ebp == 0) {
		unhandledException(returnAddress, *obj);
	}
	
	// Set throw obj
	SystemException** objDst = reinterpret_cast<SystemException**>(ebp + c->dispCatchObj);
	*objDst = *obj;

	// Use trick to provide EBP
	link = reinterpret_cast<ChainLink*>(ebp);

	// Invoke catch
	return c->addressOfHandler;
}
