#include <neutron.h>
#include <neutron/arch/Architecture.h>
#include <neutron/NeutronKernel.h>
#include <klibc/string.h>

#include "crt.h"

using namespace Neutron;
using namespace Neutron::Arch;

uintptr_t newHandler(size_t size) {
	uintptr_t ptr = Architecture::currentArch()->heap()->alloc(size);
	if (ptr == 0) {
		NeutronKernel::getInstance()->panic(0, "Available RAM exhausted (tried to allocate %d bytes)", size);
	}

	// Clear block
	memset(ptr, 0, size);

	return ptr;
}

void freeHandler(uintptr_t ptr) {
	Architecture::currentArch()->heap()->free(ptr);
}

uintptr_t operator new(size_t size)
{
	return newHandler(size);
}

uintptr_t operator new[](size_t size)
{
	return newHandler(size);
}

void operator delete(uintptr_t ptr) {
	freeHandler(ptr);
}

void operator delete(uintptr_t ptr, size_t size) {
	LOG_WARN("Call to undefined operator delete(uintptr_t ptr, size_t size)\n    ptr = %X\n    size = %d", ptr, size);
}

void operator delete[](uintptr_t ptr)
{
	freeHandler(ptr);
}
