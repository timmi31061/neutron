#include <neutron.h>
#include <neutron/NeutronKernel.h>

extern "C" int _fltused = 1;

extern "C" void __std_terminate() {
	Neutron::NeutronKernel::getInstance()->panic(0, "__std_terminate was called.");
}
