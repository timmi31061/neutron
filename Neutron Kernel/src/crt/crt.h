#pragma once
#include <neutron.h>

/**
* Invokes all static constructors.
*/
bool _crt_invokeConstructors();
