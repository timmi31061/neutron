.686p
.model flat
.code

;global __CxxThrowException@8

extern _CxxThrowException: PROC

__CxxThrowException@8 PROC
	; pop return address
	pop eax
	
	; push caller's ebp
	push ebp

	; push return address
	push eax

	call _CxxThrowException

	; drop return address
	add esp, 4

	; pop catch's ebp
	pop ebp

	; clean stack
	; 2 args (each 4 bytes) = 8
	add esp, 8

	; invoke catch
	call eax
	jmp eax
__CxxThrowException@8 ENDP

___CxxFrameHandler3 PROC
	ret
___CxxFrameHandler3 ENDP

;
; Implement dummy functions
;

__imp__IsDebuggerPresent@0 PROC
	mov eax, 0
	ret
__imp__IsDebuggerPresent@0 ENDP

_IsProcessorFeaturePresent@4 PROC
	mov eax, 0
	ret 4
_IsProcessorFeaturePresent@4 ENDP

__imp__EncodePointer@4 PROC
	mov eax, 0
	ret 4
__imp__EncodePointer@4 ENDP

__imp__DecodePointer@4 PROC
	mov eax, 0
	ret 4
__imp__DecodePointer@4 ENDP

_atexit PROC
	ret
_atexit ENDP

END
