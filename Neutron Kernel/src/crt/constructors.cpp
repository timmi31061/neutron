#include <neutron.h>

#include "crt.h"

// Constructor prototypes
typedef void(__cdecl *_PVFV)(void);
typedef int(__cdecl *_PIFV)(void);

// Linker puts constructors between these sections, and we use them to locate constructor pointers.
#pragma section(".CRT$XIA",long,read)
#pragma section(".CRT$XIZ",long,read)
#pragma section(".CRT$XCA",long,read)
#pragma section(".CRT$XCZ",long,read)

// Put .CRT data into .rdata section
#pragma comment(linker, "/merge:.CRT=.rdata")

// Pointers surrounding constructors
__declspec(allocate(".CRT$XIA")) _PIFV __xi_a[] = { 0 };
__declspec(allocate(".CRT$XIZ")) _PIFV __xi_z[] = { 0 };
__declspec(allocate(".CRT$XCA")) _PVFV __xc_a[] = { 0 };
__declspec(allocate(".CRT$XCZ")) _PVFV __xc_z[] = { 0 };

extern __declspec(allocate(".CRT$XIA")) _PIFV __xi_a[];
extern __declspec(allocate(".CRT$XIZ")) _PIFV __xi_z[];    // C initializers
extern __declspec(allocate(".CRT$XCA")) _PVFV __xc_a[];
extern __declspec(allocate(".CRT$XCZ")) _PVFV __xc_z[];    // C++ initializers

/**
 * Invokes all C constructors.
 */
static int _crt_init_constructors_c(_PIFV* pfbegin, _PIFV* pfend) {
	int ret = 0;

	// Walk through the table of function pointers until the end is encountered.
	// The initial value of pfbegin points to the first valid entry. Only entries before pfend are valid.
	while (pfbegin < pfend  && ret == 0) {
		// if current table entry is not NULL, call it.
		if (*pfbegin != 0) {
			ret = (**pfbegin)();
		}
		++pfbegin;
	}

	return ret;
}

/**
 * Invokes all C++ constructors.
 */
static void _crt_init_constructors_cpp(_PVFV* pfbegin, _PVFV* pfend) {

	// Walk through the table of function pointers until the end is encountered.
	// The initial value of pfbegin points to the first valid entry. Only entries before pfend are valid.
	while (pfbegin < pfend) {
		// if current table entry is not NULL, call it.
		if (*pfbegin != 0) {
			(**pfbegin)();
		}
		++pfbegin;
	}
}

/**
 * Invokes all static constructors.
 */
bool _crt_invokeConstructors() {
	// Do C initialization
	int initret = _crt_init_constructors_c(__xi_a, __xi_z);
	if (initret != 0) {
		return false;
	}

	// Do C++ initialization
	_crt_init_constructors_cpp(__xc_a, __xc_z);
	return true;
}