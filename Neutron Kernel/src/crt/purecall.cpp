#include <neutron.h>
#include <neutron/NeutronKernel.h>

extern "C" int _purecall() {
	Neutron::NeutronKernel::getInstance()->panic(0, "Call to pure virtual function.");
	return 0;
}
