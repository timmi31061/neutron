#pragma once
#include <neutron/exceptions/OutOfRangeException.h>

// Outer for is executed one time (inner for returns, when invalid).
// It ist just used to hide __enumerator from the scope.
#define FOREACH(enumerable, item) \
for (auto __enumerator_##item = (enumerable)->enumerator(); __enumerator_##item; delete __enumerator_##item, __enumerator_##item = 0) \
	for ( \
		auto item = __enumerator_##item->current(); \
		__enumerator_##item->isValid() && ((item = __enumerator_##item->current()) != 0 || true); \
		__enumerator_##item->next() \
	)

namespace kstl {
	template<typename T>
	class IEnumerator {
	public:
		virtual bool next() = 0;
		virtual bool isValid() = 0;
		virtual T current() = 0;
	};

	template<typename T>
	class ArrayEnumerator : public IEnumerator<T> {
	public:
		ArrayEnumerator(T* array, int length) {
			_array = array;
			_length = length;
		}

		virtual bool next() {
			_index++;
			return isValid();
		}

		virtual bool isValid() {
			return _index < _length;
		}

		virtual T current() {
			if (_index >= _length) {
				THROW_OUT_OF_RANGE();
			}

			return _array[_index];
		}

	protected:
		T* _array;
		int _length;
		int _index;
	};

	template<typename T>
	class IEnumerable {
	public:
		virtual IEnumerator<T>* enumerator() = 0;
	};
}
