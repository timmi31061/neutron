#pragma once
#include <neutron.h>
#include <klibc/string.h>
#include <klibc/kprintf.h>
#include <klibc/typeinfo.h>
#include <neutron/exceptions/OutOfRangeException.h>

#include "enumerator.h"

namespace kstl {
	template <typename T>
	class IList : public IEnumerable<T> {
	public:
		virtual void clear() = 0;
		virtual void add(T obj) = 0;
		virtual void removeAt(uint32_t index) = 0;
		virtual T& item(uint32_t index) = 0;
		virtual uint32_t capacity() = 0;
		virtual uint32_t length() = 0;
		virtual bool contains(T val) = 0;

		virtual T& operator[](uint32_t index) = 0;
		virtual IList<T>& operator<<(T value) = 0;
	};

	template <typename T>
	class ArrayListBase : public IList<T> {
	public:
		virtual ~ArrayListBase() {
			clear();
		}

		virtual IEnumerator<T>* enumerator() {
			return new ArrayEnumerator<T>(_contents, _length);
		}

		virtual inline void clear() {
			_capacity = this->_length = 0;
			delete[] _contents;
		}

		virtual void add(T obj) {
			if (this->_length < _capacity) {
				// There is enough space, so we can add the new element directly.
			}
			else {
				// Calculate new capacity
				uint32_t newCapacity = _capacity * 2;
				if (newCapacity == 0) {
					newCapacity = 64;
				}

				// Save old array pointer
				T* oldConts = _contents;

				// Create new array
				_contents = new T[newCapacity];
				_capacity = newCapacity;

				if (this->_length) {
					// Copy the old contents to it
					memcpy(_contents, oldConts, sizeof(T) * _length);
					// Delete old array
					delete[] oldConts;
				}

				// Finally, add the new element
			}

			// Add the new element
			this->_contents[_length++] = obj;
		}

		virtual void removeAt(uint32_t index) {
			if (index == _length - 1) {
				// We're lucky - it is the last element, so we just have to decrement the length!
			}
			else {
				// We have to copy all following elements one element forward...
				memcpy(&_contents[index], &_contents[index + 1], sizeof(T) * (_length - index - 1));

				// ...and then to decement the length.
			}

			_length--;
		}

		virtual inline T& item(uint32_t index) {
			if (index >= _length) {
				THROW_OUT_OF_RANGE();
			}

			return this->_contents[index];
		}

		virtual inline uint32_t capacity() {
			return _capacity;
		}

		virtual inline uint32_t length() {
			return _length;
		}

		virtual bool contains(T val) {
			for (uint32_t i = 0; i < this->_length; i++) {
				if (isEqual(this->operator[](i), val)) {
					return true;
				}
			}
			return false;
		}

		virtual inline T& operator[](uint32_t index) {
			return item(index);
		}

		virtual IList<T>& operator<<(T value) {
			add(value);
			return *this;
		}

	protected:
		virtual inline bool isEqual(T key1, T key2) {
			return key1 == key2;
		}

	private:
		uint32_t _capacity = 0;
		uint32_t _length = 0;
		T* _contents;
	};

	template <typename T>
	class ArrayList : public ArrayListBase<T> {
	};

	template <>
	class ArrayList<char*> : public ArrayListBase<char*> {
	protected:
		virtual inline bool isEqual(char* key1, char* key2) {
			return strcmp(key1, key2) == 0;
		}
	};
}
