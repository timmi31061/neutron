#pragma once
#include <neutron.h>
#include <klibc/string.h>
#include <neutron/exceptions/SystemException.h>
#include "enumerator.h"
#include "list.h"

namespace kstl {
	template <typename TKey, typename TValue>
	class KeyValuePair {
	public:
		KeyValuePair<TKey, TValue>(TKey key, TValue value) {
			_key = key;
			_value = value;
		}

		TKey key() {
			return _key;
		}

		TValue value() {
			return _value;
		}

	private:
		TKey _key;
		TValue _value;
	};

	template <typename TKey, typename TValue>
	class IMap : public IEnumerable<KeyValuePair<TKey, TValue>*> {
	public:
		virtual void clear() = 0;
		virtual void add(TKey key, TValue value) = 0;
		virtual void removeAt(uint32_t index) = 0;
		virtual void remove(TKey key) = 0;
		virtual TValue& value(TKey key) = 0;
		virtual TValue& valueAt(uint32_t index) = 0;
		virtual KeyValuePair<TKey, TValue> itemAt(uint32_t index) = 0;
		virtual TKey& keyAt(uint32_t index) = 0;
		virtual uint32_t findIndex(TKey key) = 0;
		virtual uint32_t length() = 0;

		virtual TValue& operator[](TKey key) = 0;
		virtual IMap<TKey, TValue>& operator<<(KeyValuePair<TKey, TValue> pair) = 0;
	};

	template<typename TKey, typename TValue>
	class MapEnumerator : public IEnumerator<KeyValuePair<TKey, TValue>*> {
	public:
		virtual ~MapEnumerator() { }

		MapEnumerator<TKey, TValue>(IMap<TKey, TValue>* map) {
			_map = map;
		}

		virtual bool next() {
			_index++;
			return isValid();
		}

		virtual bool isValid() {
			return _index < _map->length();
		}

		virtual KeyValuePair<TKey, TValue>* current() {
			return new KeyValuePair<TKey, TValue>(_map->keyAt(_index), _map->valueAt(_index));
		}

	private:
		IMap<TKey, TValue>* _map;
		uint32_t _index;
	};

	template <typename TKey, typename TValue>
	class LinearMapBase : public IMap<TKey, TValue> {
	public:
		virtual ~LinearMapBase() { }

		virtual IEnumerator<KeyValuePair<TKey, TValue>*>* enumerator() {
			return new MapEnumerator<TKey, TValue>(this);
		}

		virtual inline void clear() {
			_keys.clear();
			_values.clear();
		}

		virtual void add(TKey key, TValue value) {
			if (_keys.contains(key)) {
				THROW(Neutron::Exceptions::SystemException, "Key already exists");
			}

			_keys.add(key);
			_values.add(value);
		}

		virtual inline void removeAt(uint32_t index) {
			_keys.removeAt(index);
			_values.removeAt(index);
		}

		virtual inline void remove(TKey key) {
			this->removeAt(findIndex(key));
		}

		virtual inline TValue& value(TKey key) {
			return valueAt(findIndex(key));
		}

		virtual inline TValue& valueAt(uint32_t index) {
			return _values[index];
		}

		virtual inline KeyValuePair<TKey, TValue> itemAt(uint32_t index) {
			return KeyValuePair<TKey, TValue>(keyAt(index), valueAt(index));
		}

		virtual inline TKey& keyAt(uint32_t index) {
			return _keys[index];
		}

		virtual uint32_t findIndex(TKey key) {
			for (uint32_t i = 0; i < _keys.length(); i++) {
				if (isEqual(key, _keys.item(i))) {
					return i;
				}
			}

			THROW(Neutron::Exceptions::SystemException, "Key not found");
		}

		virtual inline uint32_t length() {
			return _keys.length();
		}

		virtual TValue& operator[](TKey key) {
			return value(key);
		}

		virtual IMap<TKey, TValue>& operator<<(KeyValuePair<TKey, TValue> pair) {
			add(pair.key(), pair.value());
			return *this;
		}

	protected:
		virtual inline bool isEqual(TKey key1, TKey key2) {
			return key1 == key2;
		}

		ArrayList<TKey> _keys;
		ArrayList<TValue> _values;
	};
	
	template <typename TKey, typename TValue>
	class LinearMap : public LinearMapBase<TKey, TValue> {
	};

	template <typename TValue>
	class LinearMap<char*, TValue> : public LinearMapBase<char*, TValue> {
	protected:
		virtual inline bool isEqual(char* key1, char* key2) {
			return strcmp(key1, key2) == 0;
		}
	};
}
