#include <neutron.h>
#include <kiostream.h>
#include <klibc/string.h>
#include <klibc/itoa.h>
#include <neutron/exceptions/SystemException.h>
#include <neutron/exceptions/NotImplementedException.h>
#include <kstl/map.h>
#include <neutron/NeutronKernel.h>
#include "pe.h"

#define THROW_INVALID_IMAGE() THROW(Neutron::Exceptions::SystemException, "Invalid PE Image.")

namespace Neutron {
	namespace PE {
		PeImage::PeImage(uint32_t fileAddress, bool skipLoading, uint32_t baseAddress) : _fileAddress(fileAddress), _baseAddress(baseAddress) {
			// Load and check MZ-EXE-Header
			ExeHeader* exe = reinterpret_cast<ExeHeader*>(_fileAddress);
			if (exe->Magic != 0x5a4d || exe->HeaderSize != 4) {
				THROW_INVALID_IMAGE();
			}

			// Load and check PE-Headers
			ImageHeader* header = reinterpret_cast<ImageHeader*>(_fileAddress + exe->PeOffset);
			_header = &header->Header;
			_optionalHeader = &header->OptionalHeader;
			if (header->Magic != 0x00004550 || _header->SizeOfOptionalHeader < 96) {
				THROW_INVALID_IMAGE();
			}

			if (_optionalHeader->Magic != 0x10b) {
				THROW_NOT_IMPLEMENTED_MSG("Invalid PE Image: PE32+ not supported.");
			}

			if (!skipLoading) {
				loadSections();
				relocate();
				scanExports();
				scanImports();
			}
		}

		void PeImage::loadSections() {
			// Calculate base address
			if (_baseAddress == 0) {
				_baseAddress = (uint32_t)Arch::Architecture::currentArch()->pmm()->alloc(CEILDIV(_optionalHeader->SizeOfImage, Arch::Architecture::currentArch()->pageSize()));
				//_baseAddress = 0xF00000;
				//_baseAddress = opt->ImageBase;
			}

			// Load sections
			PeSection* sections = reinterpret_cast<PeSection*>(reinterpret_cast<uint32_t>(_optionalHeader) + _header->SizeOfOptionalHeader);
			for (int i = 0; i < _header->NumberOfSections; i++) {
				PeSection* sec = &sections[i];

				uint32_t file = _fileAddress + sec->PointerToRawData;
				uint32_t virt = _baseAddress + sec->VirtualAddress;

				if (sec->PointerToRawData != 0) {
					memcpy((uintptr_t)virt, (uintptr_t)file, sec->SizeOfRawData);
				}

				if (sec->VirtualSize > sec->SizeOfRawData) {
					uint32_t sizeDiff = sec->VirtualSize - sec->SizeOfRawData;
					uint32_t dataEnd = sec->VirtualAddress + sec->VirtualSize;
					memset((uintptr_t)(_baseAddress + dataEnd), 0, sizeDiff);
				}
			}
		}

		void PeImage::relocate() {
			// Relocate, if necessary
			int32_t relocOffset = _baseAddress - _optionalHeader->ImageBase;
			if (relocOffset) {
				PeDataDirectory* relocDirectory = &_optionalHeader->Directories[5];

				// Verify that the relocation directory exists.
				if (_optionalHeader->DirectoryCount < 6 || relocDirectory->Size == 0) {
					THROW(Neutron::Exceptions::SystemException, "Unable to load PE image: Relocation failed.");
				}

				// Calculate address of first relocation block
				PeRelocationBase* relocBase = reinterpret_cast<PeRelocationBase*>(relocDirectory->VirtualAddress + _baseAddress);

				// Loop over the blocks until the end of the section is reached.
				while ((uint32_t)relocBase < relocDirectory->VirtualAddress + relocDirectory->Size + _baseAddress) {
					// Calculate address of block data
					PeRelocationEntry* reloc = reinterpret_cast<PeRelocationEntry*>((uint32_t)relocBase + sizeof(PeRelocationBase));

					// Loop over the entries until the end of the block is reached and do relocations.
					while ((uint32_t)reloc < ((uint32_t)relocBase + relocBase->BlockSize)) {
						uint32_t dstAddr = (_baseAddress + relocBase->VirtualAddress + reloc->offset);

						switch (reloc->type) {
							// IMAGE_REL_BASED_ABSOLUTE
							// Padding.
						case 0:
							break;

							// IMAGE_REL_BASED_HIGH
							// The base relocation adds the high 16 bits of the difference to the 16-bit field at offset.
							// The 16-bit field represents the high value of a 32-bit word.
						case 1:
						{
							uint16_t* dst = reinterpret_cast<uint16_t*>(dstAddr);
							*dst += relocOffset >> 16;
							break;
						}

						// IMAGE_REL_BASED_LOW
						// The base relocation adds the low 16 bits of the difference to the 16-bit field at offset.
						// The 16-bit field represents the low half of a 32-bit word.
						case 2:
						{
							uint16_t* dst = reinterpret_cast<uint16_t*>(dstAddr);
							*dst += relocOffset & 0xFFFF;
							break;
						}

						// IMAGE_REL_BASED_HIGHLOW
						// The base relocation applies all 32 bits of the difference to the 32-bit field at offset.
						case 3:
						{
							uint32_t* dst = reinterpret_cast<uint32_t*>(dstAddr);
							*dst += relocOffset;
							break;
						}

						// Unknown type
						default:
							THROW_NOT_IMPLEMENTED_MSG("Unable to load PE image: Relocation failed: Unable to handle unknown type.");
						}

						// Next relocation.
						reloc++;
					}

					// The current address of reloc is the address of the next block.
					relocBase = reinterpret_cast<PeRelocationBase*>(reloc);
				}
			}
		}

		void PeImage::scanExports() {
			// Scan exports
			// Verify that the export directory exists.
			PeDataDirectory* exportDirectory = &_optionalHeader->Directories[0];
			if (_optionalHeader->DirectoryCount < 1 || exportDirectory->Size > 0) {
				uint32_t exportDirectoryStart = _baseAddress + exportDirectory->VirtualAddress;
				uint32_t exportDirectoryEnd = exportDirectoryStart + exportDirectory->Size;

				PeExports* exports = reinterpret_cast<PeExports*>(_baseAddress + exportDirectory->VirtualAddress);
				//kprintf("DLL %s exports:\n", _baseAddress + exports->Name);

				uint32_t* namePointers = reinterpret_cast<uint32_t*>(_baseAddress + exports->NamesRVA);
				uint32_t* addressPointers = reinterpret_cast<uint32_t*>(_baseAddress + exports->FunctionsRVA);
				for (uint32_t i = 0; i < exports->NamesCount; i++) {
					if (addressPointers[i] > exportDirectoryStart && addressPointers[i] < exportDirectoryEnd) {
						THROW_NOT_IMPLEMENTED_MSG("DLL forwarding is not implemented!");
					}

					//kprintf("  %s: %X\n", _baseAddress + namePointers[i], addressPointers[i]);
					_exportedFunctions.add(reinterpret_cast<char*>(_baseAddress + namePointers[i]), _baseAddress + addressPointers[i]);
				}
			}
		}

		void PeImage::scanImports() {
			// Scan imports
			// Verify that the import directory exists.
			PeDataDirectory* importDirectory = &_optionalHeader->Directories[1];
			if (_optionalHeader->DirectoryCount < 2 || importDirectory->Size > 0) {
				PeImports* imports = reinterpret_cast<PeImports*>(_baseAddress + importDirectory->VirtualAddress);

				PeThunk* thunk = reinterpret_cast<PeThunk*>(_baseAddress + imports->AddressTable);
				uint32_t* thunkVal = reinterpret_cast<uint32_t*>(thunk);
				
				auto dllImports = new kstl::LinearMap<char*, uint32_t*>();
				while (*thunkVal) {
					if (thunk->ImportByOrdinal) {
						THROW_NOT_IMPLEMENTED_MSG("Ordinals are not implemented!");
					}

					PeImportByName* name = reinterpret_cast<PeImportByName*>(_baseAddress + thunk->OrdinalOrNameHint);
					char* nameStr = new char[strlen(&name->Name)];
					strcpy(nameStr, &name->Name);
					dllImports->add(nameStr, thunkVal);

					thunk++;
					thunkVal++;
				}

				_importedFunctions.add(reinterpret_cast<char*>(_baseAddress + imports->Name), dllImports);
			}
		}

		void PeImage::linkImports(kstl::LinearMap<char*, kstl::LinearMap<char*, uint32_t>*>* libraries) {
			// Link imports
			FOREACH(&(_importedFunctions), i) {
				//kprintf("DLL %s\n", i->key());

				kstl::LinearMap<char*, uint32_t>* lib = libraries->value(i->key());

				FOREACH(i->value(), j) {
					uint32_t* funcPtr = j->value();

					try {
						*funcPtr = lib->value(j->key());
					}
					catch (...) {
						THROW(Neutron::Exceptions::SystemException, "Unable to load PE image: Dynamic linking failed.");
					}

					//kprintf("  %X %s\n", *j->value(), j->key());
				}
			}
		}

		uint32_t PeImage::entryPoint() {
			return _baseAddress + _optionalHeader->AddressOfEntryPoint;
		}
	}
}
