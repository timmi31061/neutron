#pragma once
#include <neutron.h>
#include <kstl/map.h>

namespace Neutron {
	namespace PE {
		struct ExeHeader {
			uint16_t Magic; // 0x5a4d
			uint8_t _unused0[6];
			uint16_t HeaderSize; // in paragraphs (16 bytes); must be 4
			uint8_t _unused1[50];
			uint32_t PeOffset;
		};

		struct PeHeader {
			uint16_t Machine;
			uint16_t NumberOfSections;
			uint32_t TimeDateStamp;
			uint32_t PointerToSymbolTable;
			uint32_t NumberOfSymbols;
			uint16_t SizeOfOptionalHeader;
			uint16_t Characteristics;
		};

		struct PeDataDirectory {
			uint32_t VirtualAddress;
			uint32_t Size;
		};

		struct PeOptionalHeader {
			uint16_t Magic; // 0x010b - PE32, 0x020b - PE32+ (64 bit)
			uint8_t  LinkerMajorVersion;
			uint8_t  LinkerMinorVersion;
			uint32_t SizeOfCode;
			uint32_t SizeOfInitializedData;
			uint32_t SizeOfUninitializedData;
			uint32_t AddressOfEntryPoint;
			uint32_t BaseOfCode;
			uint32_t BaseOfData;
			uint32_t ImageBase;
			uint32_t SectionAlignment;
			uint32_t FileAlignment;
			uint16_t MajorOperatingSystemVersion;
			uint16_t MinorOperatingSystemVersion;
			uint16_t MajorImageVersion;
			uint16_t MinorImageVersion;
			uint16_t MajorSubsystemVersion;
			uint16_t MinorSubsystemVersion;
			uint32_t Win32VersionValue;
			uint32_t SizeOfImage;
			uint32_t SizeOfHeaders;
			uint32_t CheckSum;
			uint16_t Subsystem;
			uint16_t DllCharacteristics;
			uint32_t SizeOfStackReserve;
			uint32_t SizeOfStackCommit;
			uint32_t SizeOfHeapReserve;
			uint32_t SizeOfHeapCommit;
			uint32_t LoaderFlags;

			uint32_t DirectoryCount;
			PeDataDirectory Directories[16];
		};

		struct PeSection { // size 40 bytes
			char Name[8];
			uint32_t VirtualSize;
			uint32_t VirtualAddress;
			uint32_t SizeOfRawData;
			uint32_t PointerToRawData;
			uint32_t PointerToRelocations;
			uint32_t PointerToLinenumbers;
			uint16_t NumberOfRelocations;
			uint16_t NumberOfLinenumbers;
			uint32_t Characteristics;
		};

		struct PeRelocationBase {
			uint32_t VirtualAddress;
			uint32_t BlockSize;
		};

		struct PeRelocationEntry {
			uint16_t offset : 12;
			uint16_t type : 4;
		};

		struct PeExports {
			// Reserved, must be 0.
			uint32_t Flags;

			uint32_t TimeStamp;
			uint16_t MajorVersion;
			uint16_t MinorVersion;

			// The RVA of the ASCII string that contains the name of the DLL.
			uint32_t Name;

			// The starting ordinal number for exports in this image. This field specifies the
			// starting ordinal number for the export address table. It is usually set to 1.
			uint32_t OrdinalBase;

			// The number of entries in the export address table.
			uint32_t FunctionsCount;

			// The number of entries in the name pointer table. This is also the number of
			// entries in the ordinal table.
			uint32_t NamesCount;

			// The address of the export address table.
			uint32_t FunctionsRVA;

			// The address of the export name pointer table.
			uint32_t NamesRVA;

			// The address of the ordinal table.
			uint16_t NameOrdinalRVA;
		};

		struct PeImports {
			// RVA of the lookup table
			uint32_t LookupTable;

			// The stamp that is set to zero until the image is bound.
			// After the image is bound, this field is set to the time/data stamp of the DLL
			uint32_t  TimeDateStamp;

			// The index of the first forwarder reference.
			uint32_t  ForwarderChain;

			// RVA to DLL Name string
			uint32_t  Name;

			// The RVA of the import address table. The contents of this table are identical
			// to the contents of the import lookup table until the image is bound.
			uint32_t  AddressTable;
		};

		struct PeThunk {
			/*union {
				// A 31-bit RVA of a hint/name table entry
				uint32_t NameHint : 31;

				//A 16-bit ordinal number. 
				uint32_t Ordinal : 16;
			};*/

			// A 31-bit RVA of a hint/name table entry
			// == OR ==
			// A 16-bit ordinal number. 
			uint32_t OrdinalOrNameHint : 31;

			// If this bit is set, import by ordinal. Otherwise, import by name.
			uint32_t ImportByOrdinal : 1;
		};
		
		struct PeImportByName {
			// An index into the export name pointer table.
			uint16_t Hint;

			// First char of the import name.
			char Name;
		};

		struct ImageHeader {
			uint32_t Magic; // PE\0\0 or 0x00004550

			PeHeader Header;
			PeOptionalHeader OptionalHeader;
		};

		class NEUTRON_MODULE_API PeImage {
		public:
			PeImage(uint32_t fileAddress, bool skipLoading = false, uint32_t baseAddress = 0);

			void loadSections();
			void relocate();
			void scanExports();
			void scanImports();
			void linkImports(kstl::LinearMap<char*, kstl::LinearMap<char*, uint32_t>*>* libraries);

			uint32_t entryPoint();

			PeHeader* _header;
			PeOptionalHeader* _optionalHeader;
			PeSection* _sections;
			PeExports* _exports;
			PeImports* _imports;
			uint32_t _baseAddress;

			kstl::LinearMap<char*, uint32_t> _exportedFunctions;
			kstl::LinearMap<char*, kstl::LinearMap<char*, uint32_t*>*> _importedFunctions;

		private:
			uint32_t _fileAddress;
		};
	}
}
