#include <kiostream.h>
#include <neutron/arch/Architecture.h>
#include <neutron/driver/DriverManager.h>
#include <neutron/logging/NullLogger.h>
#include "NeutronKernel.h"

#include <neutron/driver/arch/x86/hid/TtyDriver.h>
#include <neutron/driver/arch/x86/storage/FloppyDriver.h>

#include <util/dumpMem.h>
#include <neutron/multitasking/Thread.h>
#include <neutron/arch/x86/CpuStateX86.h>

#include <kstl/list.h>

using namespace Neutron::Arch;
using namespace Neutron::Arch::X86;
using namespace Neutron::Driver;
using namespace Neutron::Driver::Arch::X86;
using namespace Neutron::Logging;
using namespace Neutron::Multitasking;

namespace Neutron {
	static NeutronKernel* kernelInstance;
	static NullLogger nullLogger;
	static char panicBuffer[4096];

	uint32_t a, b;

	void taskA() {
		while (1) {
			a++;
			kout << "a";

			// Ring 0/3 test
			if (a == 400) {
				//_asm int 48;
				_asm cli;
			}
			/*if (a == 400) {
				_asm sti;
			}*/

			for (int i = 0; i < 800000; i++) {
				_asm pause;
			}
		}
	}

	void taskB() {
		while (1) {
			b++;
			kout << "B";

			// Ring 0/3 test
			/*if (b == 100) {
				_asm cli;
			}
			if (b == 500) {
				_asm sti;
			}*/
			if (b == 200) {
				//NeutronKernel::getInstance()->scheduler()->createThread(&taskA, Architecture::currentArch()->kernelContext(), false);
			}

			for (int i = 0; i < 800000; i++) {
				_asm pause;
			}
		}
	}

	uint32_t syscallDelegator(uint32_t function, uintptr_t param) {
		return kernelInstance->syscall(function, param);
	}

	void timerTickDelegator(uint64_t ticks) {
		kernelInstance->timerTick(ticks);
	}

	Scheduler* NeutronKernel::scheduler() {
		return _scheduler;
	}

	ILogger* NeutronKernel::getLogger() {
		return _logger;
	}

	void NeutronKernel::setLogger(ILogger* logger) {
		if (logger) {
			_logger = logger;
		}
	}

	NeutronKernel* NeutronKernel::getInstance() {
		return kernelInstance;
	}

	
	NeutronKernel::NeutronKernel() {
		// WARNING: This is executed before kernel initialization!
		// No memory managers are enabled!

		kernelInstance = this;
		setLogger(&nullLogger);
	}

	ManualResetEvent* mre;
	uint32_t block = 0;

	void NeutronKernel::init() {
		// Register handlers
		Architecture::currentArch()->registerSyscallHandler(syscallDelegator);
		Architecture::currentArch()->registerTimerHandler(timerTickDelegator);

		// Create scheduler
		_scheduler = new Scheduler();

		// Initialize core drivers
		kout << "Loading core drivers...\n";
		new DriverManager(16);
		DriverManager::getInstance()->loadDriver<Driver::Arch::X86::HID::TtyDriver>();
		auto drv = DriverManager::getInstance()->loadDriver<Driver::Arch::X86::Storage::FloppyDriver>();

		LOG_INFO("Test FDD read");
		char* data = new char[512];
		drv->beforeAccess();
		drv->readBlocks(data, 36, 1);
		drv->afterAccess();
		//kprintf("Data: %X %s\n", *reinterpret_cast<uint32_t*>(data), data);

		kprintf("\n");
		dumpMem(reinterpret_cast<uint32_t>(data), 128);
		kprintf("\n");
		dumpMem(reinterpret_cast<uint32_t>(data) + 384, 128);

		mre = new ManualResetEvent();
		uint8_t* d = new uint8_t[512];
		while (1) {
			mre->waitOne();
			mre->reset();

			DriverManager::getInstance()->getDriver<Neutron::Driver::Arch::X86::Storage::FloppyDriver>(false)->beforeAccess();
			DriverManager::getInstance()->getDriver<Neutron::Driver::Arch::X86::Storage::FloppyDriver>(false)->readBlocks(d, block, 1);
			//DriverManager::getInstance()->getDriver<Neutron::Driver::Arch::X86::Storage::FloppyDriver>(false)->afterAccess();

			Architecture::currentArch()->console()->clear();

			kprintf("\nBlock: %d\n\n", block);
			dumpMem(reinterpret_cast<uint32_t>(d), 128);
			kprintf("\n");
			dumpMem(reinterpret_cast<uint32_t>(d) + 384, 128);

		}

		//Architecture::currentArch()->vmm()->createUserContext()->enable();


		// Initialize modules
		Thread* ta = _scheduler->createThread(&taskA, Architecture::currentArch()->kernelContext(), false);
		Thread* tb = _scheduler->createThread(&taskB, Architecture::currentArch()->kernelContext(), true);

		ta->slotsPerRound = 1;
		tb->slotsPerRound = 1;

		LOG_INFO("ta %d", ta->threadID);
		LOG_INFO("tb %d", tb->threadID);
		//_scheduler->createThread(&taskB, Architecture::currentArch()->kernelContext(), true);
		//tb->slotsPerRound = 3;

		//_multitaskingEnabled = true;

		kout << "Hello World from Neutron Kernel!\n";
		//for (;;) _asm hlt;
	}

	void NeutronKernel::enableMultitasking() {
		_multitaskingEnabled = true;
	}

	uint64_t NeutronKernel::tickCount() {
		return this->_ticks;
	}

	void NeutronKernel::delayMs(uint32_t ms) {
		Architecture::currentArch()->delayMs(ms);
	}

	void NeutronKernel::panic(uint32_t code, const char* format, ...) {
		Architecture::currentArch()->preparePanic();

		va_list args;
		va_start(args, format);
		kvsprintf(reinterpret_cast<char*>(&panicBuffer), format, args);
		va_end(args);

#ifndef DEBUG
		kout << ConsoleColor::White << BackgroundConsoleColor(ConsoleColor::Red);
		kout.clear();
		kout.disableCursor();
		kout << "\n" << "\033cNeutron crashed. No one can hear your screams." << "\n\n\n";

		kprintf("*** STOP(%X): ", code);
		kprintf(panicBuffer);

		kout << "\n\n" << "\033cA fatal error ocurred. Neutron had to be shut down." << "\n";
		kout << "\033cAll unsaved work might be lost. We are sorry for the inconvenience." << "\n\n\n";
		kout << "\033cLook at you, sailing through the air majestically" << "\n";
		kout << "\033clike an eagle... piloting a blimp.";
#else
		kout << ConsoleColor::Red;
		kprintf("\n\nKERNEL PANIC (%X): ", code);
		kprintf(panicBuffer);
#endif

		LOG_FATAL("KERNEL PANIC (%X): %s", code, &panicBuffer);

		Architecture::currentArch()->halt(true);
		for (;;);
	}

	uint32_t NeutronKernel::syscall(uint32_t function, uintptr_t param) {
		_scheduler->currentThread()->taskState = ThreadState::Aborted;
		/*while (1) {
			kprintf("  SYSCALL  ");
			for (int i = 0; i < 240000; i++) {
				_asm pause;
			}
		}*/
		return 0;
	}

	void NeutronKernel::timerTick(uint64_t ticks) {
		this->_ticks = ticks;
		
		uint8_t col = kout.col;
		uint8_t row = kout.row;

		kout.col = 0;
		kout.row = 0;
		kout << "  Ticks: " << (uint32_t)this->_ticks << "\tMemory: " << Architecture::currentArch()->pmm()->usageKB() << " KB  ";
		kout.col = col;
		kout.row = row;

		if (_scheduler && _multitaskingEnabled) {
			Architecture::currentArch()->setCpuState(_scheduler->schedule());
		}
	}
}
