#include "InitializationInfo.h"

namespace Neutron {
	namespace Arch {
		InitializationInfo::InitializationInfo(mb_struct* multibootInfo) {
			this->multiboot = multibootInfo;
		}

		mb_struct* InitializationInfo::multibootInfo() {
			return this->multiboot;
		}
	}
}
