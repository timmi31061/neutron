#include <neutron/NeutronKernel.h>

#include "Architecture.h"

namespace Neutron {
	namespace Arch {
		static Architecture* _currentArch = 0;
		static bool _archInitialized = false;

		void Architecture::setArch(Architecture* arch) {
			if (_currentArch) {
				return;
			}

			_currentArch = arch;
		}

		void Architecture::enable(InitializationInfo* info) {
			if (!_currentArch) {
				NeutronKernel::getInstance()->panic(0, "Hardware initialization failed: No HAL bound.");
			}

			if (_archInitialized) {
				return;
			}

			_archInitialized = true;

			if (!_currentArch->init(info)) {
				NeutronKernel::getInstance()->panic(0, "Hardware initialization failed.");
			}
		}

		Architecture* Architecture::currentArch() {
			return _currentArch;
		}
	}
}
