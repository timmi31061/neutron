#pragma once
#include <neutron.h>
#include "InitializationInfo.h"
#include "VirtualMemoryContext.h"

namespace Neutron {
	namespace Arch {
		class VirtualMemoryManager
		{
		public:
			virtual void init(InitializationInfo* info) = 0;

			virtual VirtualMemoryContext* createKernelContext() = 0;
			virtual VirtualMemoryContext* createUserContext() = 0;
			virtual void enable() = 0;
		};
	}
}
