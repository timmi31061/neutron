#pragma once
#include <neutron.h>
#include <multiboot.h>

namespace Neutron {
	namespace Arch {
		class InitializationInfo final
		{
		public:
			InitializationInfo(mb_struct* multibootInfo);

			mb_struct* multibootInfo();

		private:
			mb_struct* multiboot;
		};
	}
}
