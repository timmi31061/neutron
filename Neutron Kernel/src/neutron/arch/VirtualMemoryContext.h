#pragma once
#include <neutron.h>

namespace Neutron {
	namespace Arch {
		enum class OnExistsAction {
			Ignore,
			Throw,
			Skip
		};

		class VirtualMemoryContext
		{
		public:
			VirtualMemoryContext(bool isKernelContext);

			virtual void appendPages(uintptr_t virtualAddress, uint32_t count = 1, OnExistsAction onExists = OnExistsAction::Skip) = 0;
			virtual void map(uintptr_t virtualAddress, uintptr_t physicalAddress, uint16_t flags = 0) = 0;
			virtual void unmap(uintptr_t virtualAddress) = 0;

			virtual uintptr_t getPhysicalAddress(uintptr_t virtualAddress) = 0;
			virtual uintptr_t getVirtualAddress(uintptr_t physicalAddress) = 0;
			virtual bool isPresent(uintptr_t virtualAddress) = 0;
			virtual void enable() = 0;

			virtual uintptr_t contextAddress() = 0;

			bool isKernelContext();

		private:
			bool kernelContext;
		};
	}
}
