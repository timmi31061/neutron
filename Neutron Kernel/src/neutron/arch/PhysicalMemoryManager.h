#pragma once
#include <neutron.h>
#include "InitializationInfo.h"

namespace Neutron {
	namespace Arch {
		class PhysicalMemoryManager
		{
		public:
			virtual void init(InitializationInfo* info) = 0;

			virtual uint32_t usageKB() = 0;
			virtual bool isFree(uint32_t pageNo) = 0;

			virtual uintptr_t alloc(uint32_t pageCount = 1, bool initialize = false) = 0;
			virtual void free(uintptr_t page, uint32_t pageCount = 1) = 0;
		};
	}
}
