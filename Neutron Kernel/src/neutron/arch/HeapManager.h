#pragma once
#include <neutron.h>

#define HEAP_CLUSTERSIZE 16

namespace Neutron {
	namespace Arch {
		class HeapManager
		{
		public:
			virtual void init();
			virtual uintptr_t alloc(size_t size);
			virtual void free(uintptr_t ptr);

		protected:
			virtual uintptr_t allocPages(uint32_t pageCount);

			uint32_t clustersPerPage;
			uint32_t clusterBitmapBytesPerPage;
			uint32_t clusterBitmapSize;
			uint8_t* clusterBitmap;

			uint32_t pageBitmapSize;
			uint8_t* pageBitmap;
		};

		struct HeapBlockHeader {
			uint32_t sizeInClusters;
		};
	}
}
