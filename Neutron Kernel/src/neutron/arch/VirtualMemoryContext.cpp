#include "VirtualMemoryContext.h"

namespace Neutron {
	namespace Arch {
		VirtualMemoryContext::VirtualMemoryContext(bool isKernelContext) {
			this->kernelContext = isKernelContext;
		}

		bool VirtualMemoryContext::isKernelContext() {
			return this->kernelContext;
		}
	}
}
