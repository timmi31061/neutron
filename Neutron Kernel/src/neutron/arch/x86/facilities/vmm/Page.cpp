#include <neutron.h>
#include <neutron/exceptions/SystemException.h>
#include <neutron/exceptions/OutOfRangeException.h>
#include "VirtualContext.h"

using namespace Neutron::Exceptions;

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			namespace Facilities {
				namespace VMM {
					PageEntry* Page::getEntry(uint32_t index, bool throwIfNonpresent) {
						if (index >= 1024) {
							THROW_OUT_OF_RANGE();
						}

						if (throwIfNonpresent && this->pageEntries[index].present == 0) {
							THROW(SystemException, "Entry not present.");
						}

						return &this->pageEntries[index];
					}
				}
			}
		}
	}
}

#endif // ARCH_X86
