#include <neutron.h>
//#include <klibc/string.h>
#include <neutron/arch/x86/ArchitectureX86.h>

#include "VirtualContext.h"
#include <klibc/kprintf.h>

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			namespace Facilities {
				namespace VMM {
					PageTable* PageDirectory::getTable(uint32_t index, bool throwIfNonpresent) {
						return reinterpret_cast<PageTable*>(this->getEntry(index, throwIfNonpresent)->getAddress());
					}

					void PageDirectory::map(uintptr_t virtualAddress, uintptr_t physicalAddress, uint16_t flags) {
						this->align(physicalAddress);
						this->align(virtualAddress);

						uint32_t vaddr = reinterpret_cast<uint32_t>(virtualAddress);

						uint16_t directoryIndex = (vaddr >> 22) & 0x3ff;
						uint16_t tableIndex = (vaddr >> 12) & 0x3ff;

						// Fetch entry
						PageEntry* directoryEntry = &this->pageEntries[directoryIndex];

						// When not present, alloc a new page table.
						if (directoryEntry->present == false) {
							// Set flags
							*reinterpret_cast<uint32_t*>(directoryEntry) = flags & 0xfff;
							directoryEntry->present = 1;

							if (flags == 0) {
								directoryEntry->writable = 0;
								directoryEntry->usermodeAccessable = 0;
							}

							// Alloc page and register it
							uintptr_t page = Architecture::currentArch()->pmm()->alloc(1, true);
							directoryEntry->setAddress(page);
						}

						// Fetch entry
						PageTable* table = reinterpret_cast<PageTable*>(directoryEntry->getAddress());
						PageEntry* tableEntry = &table->pageEntries[tableIndex];

						// Set flags
						*reinterpret_cast<uint32_t*>(tableEntry) = flags & 0xfff;
						tableEntry->present = 1;

						if (flags == 0) {
							tableEntry->writable = 1;
							tableEntry->usermodeAccessable = 1;
						}

						// Set address
						tableEntry->setAddress(physicalAddress);

						_asm {
							mov eax, virtualAddress;
							invlpg [eax];
						}
					}


					void PageDirectory::unmap(uintptr_t virtualAddress) {
						this->align(virtualAddress);

						uint32_t vaddr = reinterpret_cast<uint32_t>(virtualAddress);

						uint16_t directoryIndex = (vaddr >> 22) & 0x3ff;
						uint16_t tableIndex = (vaddr >> 12) & 0x3ff;

						// Fetch directory entry
						PageEntry* directoryEntry = &this->pageEntries[directoryIndex];
						if (directoryEntry->present == false) {
							// If the table isn't present, the entry isn't present anyway, so return.
							return;
						}

						// Fetch table entry
						PageTable* table = reinterpret_cast<PageTable*>(directoryEntry->getAddress());
						PageEntry* tableEntry = &table->pageEntries[tableIndex];

						// Set entry to not present (unmapped)
						tableEntry->present = 0;

						_asm {
							mov eax, virtualAddress;
							invlpg[eax];
						}
					}


					void PageDirectory::align(uintptr_t& address) {
						uint32_t a = reinterpret_cast<uint32_t>(address);
						a -= a % PAGESIZE;
						address = reinterpret_cast<uintptr_t>(a);
					}
				}
			}
		}
	}
}

#endif // ARCH_X86
