#include <neutron.h>

#include "VirtualContext.h"

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			namespace Facilities {
				namespace VMM {
					void PageEntry::setAddress(uintptr_t newAddress) {
						address = reinterpret_cast<uint32_t>(newAddress) >> 12;
					}


					uintptr_t PageEntry::getAddress() {
						return reinterpret_cast<uintptr_t>(address << 12);
					}
				}
			}
		}
	}
}

#endif // ARCH_X86
