#pragma once
#include <neutron.h>

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			namespace Facilities {
				namespace VMM {
					class PageEntry {
					public:
						uint32_t present : 1;
						uint32_t writable : 1;
						uint32_t usermodeAccessable : 1;
						uint32_t useWriteThroughChache : 1;
						uint32_t useNoCache : 1;
						uint32_t accessed : 1;
						uint32_t reserved : 3;
						uint32_t availableBits : 3;
						uint32_t address : 20;

						void setAddress(uintptr_t address);
						uintptr_t getAddress();
					};


					class Page {
					public:
						PageEntry* getEntry(uint32_t index, bool throwIfNonpresent = true);

						PageEntry pageEntries[1024];
					};


					class PageTable : public Page {
					};


					class PageDirectory : public Page {
					public:
						PageTable* getTable(uint32_t index, bool throwIfNonpresent = true);
						void map(uintptr_t virtualAddress, uintptr_t physicalAddress, uint16_t flags = 0);
						void unmap(uintptr_t virtualAddress);

					private:
						void align(uintptr_t& address);
					};
				}
			}
		}
	}
}

#endif // ARCH_X86
