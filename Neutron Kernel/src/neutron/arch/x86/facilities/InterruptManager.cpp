#include <neutron.h>
#include <neutron/NeutronKernel.h>

#include "../ArchitectureX86.h"
#include "InterruptManager.h"
#include "Gdt.h"
#include <klibc/kprintf.h>

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			// Private macros
#define REGISTER_EX(i, flg) this->registerHandler(i, CONCAT(intStub,i), GDT_KERNEL_CODESEG_SELECTOR, IDT_PRESENT | IDT_32BIT | flg)
#define REGISTER(i) REGISTER_EX(i, IDT_RING0 | IDT_INTERRUPT_GATE)

#define STUB(i)									\
__declspec(naked) void CONCAT(intStub,i)() {	\
		_asm push 0								\
		_asm push i								\
		_asm jmp commonInterruptHandler			\
					}

#define STUB_ERRCODE(i)							\
__declspec(naked) void CONCAT(intStub,i)() {	\
		_asm push i								\
		_asm jmp commonInterruptHandler			\
					}

			// Private declares
			void commonInterruptHandler();
			void interruptDelegator(CpuStateX86& cpu);

			static InterruptManager manager;


			// Interrupt bootstrapper
			InterruptDelegator interruptBootstrapper;

#pragma region handler stubs
			// Exceptions
			STUB(0);
			STUB(1);
			STUB(2);
			STUB(3);
			STUB(4);
			STUB(5);
			STUB(6);
			STUB(7);
			STUB_ERRCODE(8);
			STUB(9);
			STUB_ERRCODE(10);
			STUB_ERRCODE(11);
			STUB_ERRCODE(12);
			STUB_ERRCODE(13);
			STUB_ERRCODE(14);
			STUB(15);
			STUB(16);
			STUB_ERRCODE(17);
			STUB(18);
			STUB(19);
			STUB(20);
			STUB(21);
			STUB(22);
			STUB(23);
			STUB(24);
			STUB(25);
			STUB(26);
			STUB(27);
			STUB(29);
			STUB_ERRCODE(30);
			STUB(31);

			// IRQs
			STUB(32);
			STUB(33);
			STUB(34);
			STUB(35);
			STUB(36);
			STUB(37);
			STUB(38);
			STUB(39);
			STUB(40);
			STUB(41);
			STUB(42);
			STUB(43);
			STUB(44);
			STUB(45);
			STUB(46);
			STUB(47);

			// Syscall
			STUB(48);
#pragma endregion

			InterruptManager::InterruptManager()
			{
				Arch::X86::interruptBootstrapper = interruptDelegator;

#pragma region Register interrupt handler stubs
				// Exceptions
				REGISTER(0);
				REGISTER(1);
				REGISTER(2);
				REGISTER(3);
				REGISTER(4);
				REGISTER(5);
				REGISTER(6);
				REGISTER(7);
				REGISTER(8);
				REGISTER(9);
				REGISTER(10);
				REGISTER(11);
				REGISTER(12);
				REGISTER(13);
				REGISTER(14);
				REGISTER(15);
				REGISTER(16);
				REGISTER(17);
				REGISTER(18);
				REGISTER(19);
				REGISTER(20);
				REGISTER(21);
				REGISTER(22);
				REGISTER(23);
				REGISTER(24);
				REGISTER(25);
				REGISTER(26);
				REGISTER(27);
				REGISTER(29);
				REGISTER(30);
				REGISTER(31);

				// IRQs
				REGISTER(32);
				REGISTER(33);
				REGISTER(34);
				REGISTER(35);
				REGISTER(36);
				REGISTER(37);
				REGISTER(38);
				REGISTER(39);
				REGISTER(40);
				REGISTER(41);
				REGISTER(42);
				REGISTER(43);
				REGISTER(44);
				REGISTER(45);
				REGISTER(46);
				REGISTER(47);

				// Syscall
				REGISTER_EX(48, IDT_RING3 | IDT_TRAP_GATE);
#pragma endregion
			}

			void InterruptManager::install(InterruptHandler interruptHandler) {
				handler = interruptHandler;

				// Install new IDT
				struct {
					uint16_t limit;
					uintptr_t pointer;
				} idtp;
				idtp.pointer = idt;
				idtp.limit = IDT_SIZE;
				_asm lidt idtp;

				// Setup PIC
				this->picSetup();
			}

			void InterruptManager::enableEarlyInterrupts() {
				earlyInterrupts = true;
				_asm sti
			}

			void InterruptManager::enableInterrupts() {
				earlyInterrupts = false;
				_asm sti
			}

			void InterruptManager::disableInterrupts() {
				_asm cli
			}

			uintptr_t InterruptManager::interruptBootstrapper(CpuStateX86& cpu) {
				// Check for spurious IRQ
				//if (cpu.intr >= PIC_IRQ0 && cpu.intr <= PIC_IRQ0 + 15) {
				if (cpu.intr == PIC_IRQ0 + 7 || cpu.intr == PIC_IRQ0 + 15) {
					// Is it a spurious IRQ?
					if (this->picGetIsr() == 0) {
						// Was it triggered by slave PIC?
						if (cpu.intr >= PIC_IRQ0 + 8) {
							// Send EOI for IRQ2 to master
							// (master-PIC is processing a "real" IRQ2 from slave)
							this->picSendEoi(PIC_IRQ0 + 2);
						}

						// Increment counter
						this->spuriousIrqCounter++;

						// Ignore this IRQ
						return &cpu;
					}
				}

				CpuStateX86* newCpu = &cpu;

				// Just call exception handlers in early mode.
				if (earlyInterrupts) {
					if (cpu.intr < 0x20) {
						this->handler(cpu);
						NeutronKernel::getInstance()->panic(0, "Early interrupt handler returned.");
					}
				}
				else {
					// This is a normal IRQ or interrupt; call ISR
					newCpu = this->handler(cpu);
				}

				// Was it an IRQ?
				if (cpu.intr >= PIC_IRQ0 && cpu.intr <= PIC_IRQ0 + 15) {
					// Send EOI
					this->picSendEoi(static_cast<uint8_t>(cpu.intr));
				}

				// EDX is our "early-mode" indicator.
				// Do that double copying or otherwise the compiler
				// will put the address of earlyInterrupts in EDX.
				uint32_t early = earlyInterrupts;
				_asm mov edx, early
				return newCpu;
			}

			uint32_t InterruptManager::getSpuriousIrqCounter() {
				return this->spuriousIrqCounter;
			}


			void InterruptManager::registerHandler(uint8_t index, uintptr_t offset, uint16_t selector, uint8_t flags) {
				this->idt[index].offsetLow = reinterpret_cast<uint32_t>(offset)& 0xFFFF;
				this->idt[index].offsetHigh = reinterpret_cast<uint32_t>(offset) >> 16;
				this->idt[index].selector = selector;
				this->idt[index].flags = flags;
			}

			void InterruptManager::picSetup() {
				// ICW 1
				Architecture::currentArch()->outb(PIC_MASTER_COMMAND, 0x11);
				Architecture::currentArch()->outb(PIC_SLAVE_COMMAND, 0x11);

				// ICW 2
				Architecture::currentArch()->outb(PIC_MASTER_DATA, PIC_IRQ0);
				Architecture::currentArch()->outb(PIC_SLAVE_DATA, PIC_IRQ0 + 8);

				// ICW 3
				Architecture::currentArch()->outb(PIC_MASTER_DATA, 0x04);
				Architecture::currentArch()->outb(PIC_SLAVE_DATA, 2);

				// ICW 4
				Architecture::currentArch()->outb(PIC_MASTER_DATA, 0x01);
				Architecture::currentArch()->outb(PIC_SLAVE_DATA, 0x01);

				// Unmask interrupts
				Architecture::currentArch()->outb(PIC_MASTER_DATA, 0x00);
				Architecture::currentArch()->outb(PIC_SLAVE_DATA, 0x00);

				// Select In-Service-Register for read (important for picGetIsr)
				Architecture::currentArch()->outb(PIC_MASTER_COMMAND, PIC_ISR);
				Architecture::currentArch()->outb(PIC_SLAVE_COMMAND, PIC_ISR);
			}

			uint16_t InterruptManager::picGetIsr() {
				// Read ISR (In Service Register) of PIC
				uint16_t isr = Architecture::currentArch()->inb(PIC_MASTER_COMMAND) | (Architecture::currentArch()->inb(PIC_SLAVE_COMMAND) << 8);

				// Remove IRQ2 bit (this is the slave PIC)
				isr &= ~(0x0004);

				return isr;
			}

			void InterruptManager::picSendEoi(uint8_t intr) {
				Architecture::currentArch()->outb(PIC_MASTER_COMMAND, PIC_EOI);
				if (intr >= PIC_IRQ0 + 8) {
					Architecture::currentArch()->outb(PIC_SLAVE_COMMAND, PIC_EOI);
				}
			}

			const char* InterruptManager::exceptionName(const uint32_t exception) {
				switch (exception) {
				case 0:  return "DIVIDE_BY_ZERO";
				case 1:  return "DEBUG";
				case 2:  return "NON_MASKABLE_INTERRUPT";
				case 3:  return "BREAKPOINT";
				case 4:  return "OVERFLOW";
				case 5:  return "BOUND_RANGE";
				case 6:  return "INVALID_OPCODE";
				case 7:  return "DEVICE_NOT_AVAILABLE";
				case 8:  return "DOUBLE_FAULT";
				case 9:  return "COPROCESSOR_SEGMENT_OVERRUN";
				case 10: return "INVALID_TSS";
				case 11: return "SEGMENT_NOT_PRESENT";
				case 12: return "STACK_FAULT";
				case 13: return "GENERAL_PROTECTION_FAULT";
				case 14: return "PAGE_FAULT";
				case 15: return "INVALID_EXCEPTION";
				case 16: return "X87_FLOATING_POINT";
				case 17: return "ALIGNMENT_CHECK";
				case 18: return "MACHINE_CHECK";
				case 19: return "SIMD_FLOATING_POINT";
				case 30: return "SECURITY_SENSITIVE_EVENT_IN_HOST";
				}
				return exceptionName(15);
			}

			void interruptDelegator(CpuStateX86& cpu) {
				static_cast<ArchitectureX86*>(Architecture::currentArch())->interruptManager()->interruptBootstrapper(cpu);
			}

			__declspec(naked) void commonInterruptHandler() {
				_asm {
					push ebp;
					push edi;
					push esi;
					push edx;
					push ecx;
					push ebx;
					push eax;

					mov eax, cr3;
					push eax;

					mov ax, ds;
					mov bx, es;
					mov cx, fs;
					mov dx, gs;
					push ax;
					push bx;
					push cx;
					push dx;

					mov ax, GDT_KERNEL_DATASEG_SELECTOR;
					mov ds, ax;
					mov es, ax;
					mov fs, ax;
					mov gs, ax;

					push esp;
					call interruptBootstrapper;
					mov esp, eax;

					pop dx;
					pop cx;
					pop bx;
					pop ax;
					mov gs, dx;
					mov fs, cx;
					mov es, bx;
					mov ds, ax;

					// Only reload CR3 when necessary (it's slow)
					pop ebx;

					// Early interrupts must not modify CR3
					test edx, edx;
					jnz dontReloadCr3

					test ebx, ebx;
					jz cr3Zero;

					mov eax, cr3;
					cmp eax, ebx;
					je dontReloadCr3;
					mov cr3, ebx;

				dontReloadCr3:
					pop eax;
					pop ebx;
					pop ecx;
					pop edx;
					pop esi;
					pop edi;
					pop ebp;

					add esp, 8;

					iretd;

				cr3Zero:
				}

				uint32_t _esp;
				_asm mov _esp, esp;
				_esp -= 4; // one "pop" was executed before jump
				NeutronKernel::getInstance()->panic(_esp, "iretd: CpuState->CR3 is 0 (zero)");
			}
		}
	}
}

#endif // ARCH_X86
