#pragma once
#include <neutron.h>

#ifdef ARCH_X86

#define GDT_ENTRIES 6

#define GDT_UNIT_4K 0x800
#define GDT_32BIT 0x400

#define GDT_PRESENT 0x80
#define GDT_RING0 0x00
#define GDT_RING3 0x60
#define GDT_SEGMENT 0x10
#define GDT_EXECUTABLE 0x8
#define GDT_READWRITE 0x2

#define GDT_FLAGS_TSS (GDT_PRESENT | GDT_RING3 | 0x9)
#define GDT_FLAGS_DATASEG (GDT_PRESENT | GDT_SEGMENT | GDT_READWRITE | GDT_32BIT)
#define GDT_FLAGS_CODESEG (GDT_FLAGS_DATASEG | GDT_EXECUTABLE)

#define GDT_KERNEL_CODESEG_SELECTOR 0x8
#define GDT_KERNEL_DATASEG_SELECTOR 0x10

#define GDT_USER_CODESEG_SELECTOR 0x1b
#define GDT_USER_DATASEG_SELECTOR 0x23

#define GDT_TSS_SELECTOR 0x2B

namespace Neutron {
	namespace Arch {
		namespace X86 {
			struct Tss {
				uint32_t prev_tss;	// unused
				uint32_t esp0;		// esp to load a change to kernel mode occurs.
				uint32_t ss0;		// ss to load a change to kernel mode occurs. Everything below is unusued
				uint32_t esp1;
				uint32_t ss1;
				uint32_t esp2;
				uint32_t ss2;
				uint32_t cr3;
				uint32_t eip;
				uint32_t eflags;
				uint32_t eax;
				uint32_t ecx;
				uint32_t edx;
				uint32_t ebx;
				uint32_t esp;
				uint32_t ebp;
				uint32_t esi;
				uint32_t edi;
				uint32_t es;
				uint32_t cs;
				uint32_t ss;
				uint32_t ds;
				uint32_t fs;
				uint32_t gs;
				uint32_t ldt;
				uint16_t trap;
				uint16_t iomap_base;
			};

			class Gdt
			{
			public:
				Gdt();
				void install();

			private:
				void setEntry(uint8_t i, uint32_t base, uint32_t limit, uint16_t flags);

			private:
				uint64_t gdt[GDT_ENTRIES];
			};
		}
	}
}

#endif // ARCH_X86
