#include <neutron.h>

#include "Gdt.h"

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			static Tss tss;

			Gdt::Gdt() {
			}

#pragma warning(push)
#pragma warning(disable: 4740)
			void Gdt::install() {
				// Setup GDT
				// Null descriptor
				this->setEntry(0, 0, 0, 0);

				// Kernel code and data: 0 to 4 GiB
				this->setEntry(1, 0, 0x000fffff, GDT_FLAGS_CODESEG | GDT_UNIT_4K | GDT_RING0);
				this->setEntry(2, 0, 0x000fffff, GDT_FLAGS_DATASEG | GDT_UNIT_4K | GDT_RING0);

				// User code and data: 0 to 4 GiB
				this->setEntry(3, 0, 0x000fffff, GDT_FLAGS_CODESEG | GDT_UNIT_4K | GDT_RING3);
				this->setEntry(4, 0, 0x000fffff, GDT_FLAGS_DATASEG | GDT_UNIT_4K | GDT_RING3);

				// TSS
				tss.esp0 = KERNEL_STACK;
				tss.ss0 = GDT_KERNEL_DATASEG_SELECTOR;
				this->setEntry(5, reinterpret_cast<uint32_t>(&tss), reinterpret_cast<uint32_t>(&tss) + sizeof(Tss), GDT_FLAGS_TSS);

				struct {
					uint16_t limit;
					uintptr_t pointer;
				} gdtp;
				gdtp.limit = GDT_ENTRIES * 8 - 1;
				gdtp.pointer = &(this->gdt);

				_asm {
					// Install GDT
					lgdt gdtp;

					// Load task register
					mov ax, GDT_TSS_SELECTOR;
					ltr ax;

					// Reload segment registers
					mov ax, GDT_KERNEL_DATASEG_SELECTOR;
					mov ds, ax;
					mov es, ax;
					mov fs, ax;
					mov gs, ax;
					mov ss, ax;

					// Reload CS
					push GDT_KERNEL_CODESEG_SELECTOR;
					push __gdt_cs_fix;
					retf;
				__gdt_cs_fix:
				}
			}
#pragma warning(pop)

			void Gdt::setEntry(uint8_t i, uint32_t base, uint32_t limit, uint16_t flags) {
				this->gdt[i] = limit & 0xffffLL;
				this->gdt[i] |= (base & 0xffffffLL) << 16;
				this->gdt[i] |= (flags & 0xffLL) << 40;
				this->gdt[i] |= ((limit >> 16) & 0xfLL) << 48;
				this->gdt[i] |= ((flags >> 8) & 0xffLL) << 52;
				this->gdt[i] |= ((base >> 24) & 0xffLL) << 56;
			}

		}
	}
}

#endif // ARCH_X86
