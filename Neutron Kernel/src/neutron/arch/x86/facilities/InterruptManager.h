#pragma once
#include <neutron.h>
#include "../CpuStateX86.h"

#ifdef ARCH_X86

#define IDT_ENTRIES 49
#define IDT_SIZE (IDT_ENTRIES * 8 - 1)

#define IDT_INTERRUPT_GATE 0x06
#define IDT_TRAP_GATE 0x07
#define IDT_TASK_GATE 0x05

#define IDT_32BIT 0x08

#define IDT_RING0 0x00
#define IDT_RING1 0x20
#define IDT_RING2 0x40
#define IDT_RING3 0x60

#define IDT_PRESENT 0x80

#define PIC_MASTER_COMMAND 0x20
#define PIC_MASTER_DATA 0x21
#define PIC_SLAVE_COMMAND 0xA0
#define PIC_SLAVE_DATA 0xA1
#define PIC_EOI 0x20
#define PIC_ISR 0x0b

#define PIC_IRQ0 0x20

namespace Neutron {
	namespace Arch {
		namespace X86 {
			typedef struct {
				uint16_t offsetLow;
				uint16_t selector;
				uint8_t __reserved;
				uint8_t flags;
				uint16_t offsetHigh;
			} idt_entry_t;

			typedef CpuStateX86* (*InterruptHandler)(CpuStateX86& cpu);
			typedef void(*InterruptDelegator)(CpuStateX86& cpu);

			class InterruptManager
			{
			public:
				InterruptManager();

				void install(InterruptHandler handler);

				void enableEarlyInterrupts();
				void enableInterrupts();
				void disableInterrupts();

				uintptr_t interruptBootstrapper(CpuStateX86& cpu);

				uint32_t getSpuriousIrqCounter();

				const char* exceptionName(const uint32_t exception);

			private:
				void registerHandler(uint8_t index, uintptr_t offset, uint16_t selector, uint8_t flags);

				void picSetup();
				uint16_t picGetIsr();
				void picSendEoi(uint8_t intr);

			private:
				uint32_t spuriousIrqCounter = 0;
				idt_entry_t idt[IDT_ENTRIES];
				InterruptHandler handler;
				volatile bool earlyInterrupts;
			};
		}
	}
}

#endif // ARCH_X86
