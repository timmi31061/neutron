#pragma once
#include <neutron.h>
#include <neutron/arch/VirtualMemoryContext.h>
#include <neutron/arch/x86/facilities/vmm/VirtualContext.h>


#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			using namespace Facilities::VMM;

			class MemoryContextX86 : public VirtualMemoryContext
			{
			public:
				MemoryContextX86(bool isKernelContext);

				virtual void appendPages(uintptr_t virtualAddress, uint32_t count = 1, OnExistsAction onExists = OnExistsAction::Skip);
				virtual void map(uintptr_t virtualAddress, uintptr_t physicalAddress, uint16_t flags = 0);
				virtual void unmap(uintptr_t virtualAddress);

				virtual uintptr_t getPhysicalAddress(uintptr_t virtualAddress);
				virtual uintptr_t getVirtualAddress(uintptr_t pysicalAddress);
				virtual bool isPresent(uintptr_t virtualAddress);
				virtual void enable();

				virtual uintptr_t contextAddress();

			private:
				uint16_t getFlags();

				PageDirectory* _pageDirectory;
			};
		}
	}
}

#endif // ARCH_X86
