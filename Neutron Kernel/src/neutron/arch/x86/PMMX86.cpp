#include <neutron/arch/Architecture.h>
#include <neutron/arch/x86/ArchitectureX86.h>
#include <klibc/string.h>
#include <util/findSeq.h>
#include "PMMX86.h"

#ifdef ARCH_X86

#define SET_ALLOCATED(address) (this->bitmap[reinterpret_cast<uint32_t>(address) / PAGESIZE / PMM_ENTRYWIDTH] &=	\
	~(1 << (reinterpret_cast<uint32_t>(address) / PAGESIZE % PMM_ENTRYWIDTH)))

#define SET_FREE(address) (this->bitmap[reinterpret_cast<uint32_t>(address) / PAGESIZE / PMM_ENTRYWIDTH] |=	\
	(1 << (reinterpret_cast<uint32_t>(address) / PAGESIZE % PMM_ENTRYWIDTH)))

#define PAGE_ADDRESS(entry, bit) (reinterpret_cast<uintptr_t>((entry * PMM_ENTRYWIDTH + bit) * PAGESIZE))

namespace Neutron {
	namespace Arch {
		namespace X86{
			void PMMX86::init(InitializationInfo* info) {
				// set all pages to used (bit cleared)
				memset(this->bitmap, 0x0, sizeof(this->bitmap));

				// Iterate through the memorymap and set free blocs as free.
				uint32_t processedBytes = 0;
				char* mmap = reinterpret_cast<char*>(info->multibootInfo()->mmap);
				while (processedBytes < info->multibootInfo()->mmapLength) {
					mb_mmap* e = reinterpret_cast<mb_mmap*>(mmap);

					if (e->type == 1) { // Free
						uint32_t start = static_cast<uint32_t>(e->address);
						uint32_t end = start + static_cast<uint32_t>(e->length);

						if (end > LO_MEM) {
							start = MAX(start, LO_MEM);

							uint32_t startPage = start / PAGESIZE;
							uint32_t count = (end / PAGESIZE) - startPage;

							this->free(reinterpret_cast<uintptr_t>(start), count);
						}

					}

					processedBytes += e->structSize + 4;
					mmap += e->structSize + 4;
				}

				usedPages = 0;

				// Set multiboot structs as non-free
				SET_ALLOCATED(mbinfo);
				for (uint32_t i = 0; i < mbinfo->moduleCount; i++) {
					mb_module* mod = &mbinfo->modules[i];
					uint32_t addr = mod->startAddress;

					while (addr < mod->endAddress) {
						SET_ALLOCATED(reinterpret_cast<uintptr_t>(addr));
						usedPages++;
						addr += 0x1000;
					}
				}

				// Set kernel-mem as non-free. Good thing, trust me :>
				uint32_t kstartPage = KSTART_PAGE;
				uint32_t kendPage = KEND_PAGE;

				for (uint32_t page = kstartPage; page <= kendPage; page++) {
					SET_ALLOCATED(reinterpret_cast<uintptr_t>(page * PAGESIZE));
					usedPages++;
				}
			}

			uint32_t PMMX86::usageKB() {
				return this->usedPages * 4;
			}

			bool PMMX86::isFree(uint32_t pageNo) {
				return (this->bitmap[pageNo / PMM_ENTRYWIDTH] & (1 << (pageNo % PMM_ENTRYWIDTH))) != 0;
			}

			uintptr_t PMMX86::alloc(uint32_t pageCount, bool initialize) {
				uintptr_t result = 0;
				if (pageCount == 0) {
					// Nice joke.
					return 0;
				}
				else if (pageCount == 1) {
					// Use faster version for one page
					result = this->allocSingle();
				}
				else {
					uint32_t page = findSeq(reinterpret_cast<uint8_t*>(this->bitmap), PMM_BUFLEN, pageCount);

					// No page found
					if (page == 0) {
						return 0;
					}

					// Mark them allocated
					for (uint32_t i = 0; i < pageCount; i++) {
						SET_ALLOCATED(reinterpret_cast<uintptr_t>((i + page) * PAGESIZE));
					}

					// Increase page counter
					this->usedPages += pageCount;

					// Return address
					result = reinterpret_cast<uintptr_t>(page * PAGESIZE);
				}

				if (initialize && result != 0) {
					// Initialize page to 0x00
					memset(result, 0, PAGESIZE * pageCount);
				}

				return result;
			}

			void PMMX86::free(uintptr_t page, uint32_t pageCount) {
				this->usedPages -= pageCount;
				uint32_t startAddr = reinterpret_cast<uint32_t>(page);
				for (uint32_t i = startAddr; i < (startAddr + (pageCount * PAGESIZE)); i += PAGESIZE) {
					SET_FREE(reinterpret_cast<uintptr_t>(i));
				}
			}


			uintptr_t PMMX86::allocSingle() {
				// Find entry with free page
				for (uint32_t i = 0; i < PMM_ENTRIES; i++) {
					// If the entry is != 0, there is a free page
					if (this->bitmap[i]) {
						// Entry with free page found
						// Find page index
						for (uint8_t j = 0; j < PMM_ENTRYWIDTH; j++) {
							if (this->bitmap[i] & (1 << j)) {
								// We found the page!
								// Calculate address
								uintptr_t address = PAGE_ADDRESS(i, j);

								// Mark page as allocated
								SET_ALLOCATED(address);

								// Increase page counter
								this->usedPages++;

								// Return address
								return address;
							}
						}
					}
				}

				// No free page
				return 0;
			}
		}
	}
}

#endif // ARCH_X86
