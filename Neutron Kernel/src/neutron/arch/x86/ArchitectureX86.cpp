#include <neutron/NeutronKernel.h>
#include <neutron/driver/DriverManager.h>
#include <neutron/logging/ILogger.h>
#include <klibc/itoa.h>
#include <klibc/kprintf.h>

#include "ArchitectureX86.h"
#include "facilities/InterruptManager.h"
#include "facilities/Gdt.h"
#include "ConsoleX86.h"
#include "PMMX86.h"
#include "VMMX86.h"
#include "MemoryContextX86.h"

#ifdef ARCH_X86

using namespace Neutron::Driver;

namespace Neutron {
	namespace Arch {
		namespace X86 {
			static ArchitectureX86* archInstance;

			static ConsoleX86 _console;
			static InterruptManager _interruptManager;
			static Gdt _gdt;
			static PMMX86 _pmm;
			static VMMX86 _vmm;
			static HeapManager _heap;
			
			CpuStateX86* interruptHandlerDelegator(CpuStateX86& cpuState) {
				return archInstance->interruptHandler(cpuState);
			}

			ArchitectureX86::ArchitectureX86() {
				archInstance = this;
			}

			bool ArchitectureX86::init(InitializationInfo* info) {
				console()->init();
				console()->operator<<("Neutron platform initialization\n");

				LOG_TRACE("Installing GDT.");
				gdt()->install();

				LOG_TRACE("Initializing PMM.");
				pmm()->init(info);

				LOG_TRACE("Initializing Heap.");
				heap()->init();
				
				LOG_TRACE("Initializing interrupts.");
				interruptManager()->install(&interruptHandlerDelegator);
				interruptManager()->enableEarlyInterrupts();

				LOG_TRACE("Initializing VMM.");
				vmm()->init(info);
				_kernelContext = static_cast<MemoryContextX86*>(this->vmm()->createKernelContext());
				_kernelContext->enable();

				LOG_TRACE("Enabling VMM.");
				vmm()->enable();

				LOG_TRACE("Initializing PIT.");
				uint16_t pit = static_cast<uint16_t>(193182 / TICK_FREQ);
				outb(0x43, 0x34);
				outb(0x40, static_cast<uint8_t>(pit & 0xFF));
				outb(0x40, static_cast<uint8_t>(pit >> 8));

				LOG_TRACE("Enabling interrupts.");
				interruptManager()->enableInterrupts();

				return true;
			}

			void ArchitectureX86::registerSyscallHandler(SyscallHandler handler) {
				_syscallHandler = handler;
			}

			void ArchitectureX86::registerTimerHandler(TimerHandler handler) {
				_timerHandler = handler;
			}

			void ArchitectureX86::setCpuState(CpuState* cpuState) {
				_currentState = static_cast<CpuStateX86*>(cpuState);
			}

			CpuState* ArchitectureX86::cpuState() {
				return _currentState;
			}

			void ArchitectureX86::initializeThreadCpuState(CpuState* cpuState, uintptr_t entrypoint, uintptr_t stack, VirtualMemoryContext* context, bool kernelMode) {
				CpuStateX86* cpu = static_cast<CpuStateX86*>(cpuState);

				if (kernelMode) {
					cpu->ds = GDT_KERNEL_DATASEG_SELECTOR;
					cpu->es = GDT_KERNEL_DATASEG_SELECTOR;
					cpu->fs = GDT_KERNEL_DATASEG_SELECTOR;
					cpu->gs = GDT_KERNEL_DATASEG_SELECTOR;
					cpu->ss = GDT_KERNEL_DATASEG_SELECTOR;
					cpu->cs = GDT_KERNEL_CODESEG_SELECTOR;
				}
				else {
					cpu->ds = GDT_USER_DATASEG_SELECTOR;
					cpu->es = GDT_USER_DATASEG_SELECTOR;
					cpu->fs = GDT_USER_DATASEG_SELECTOR;
					cpu->gs = GDT_USER_DATASEG_SELECTOR;
					cpu->ss = GDT_USER_DATASEG_SELECTOR;
					cpu->cs = GDT_USER_CODESEG_SELECTOR;
				}

				cpu->eflags = 0x202;
				cpu->esp = reinterpret_cast<uint32_t>(stack);
				cpu->eip = reinterpret_cast<uint32_t>(entrypoint);
				cpu->cr3 = reinterpret_cast<uint32_t>(context->contextAddress());
			}
			
			CpuStateX86* ArchitectureX86::interruptHandler(CpuStateX86& cpuState) {
				_currentState = &cpuState;

				LOG_INTERRUPT_TRACE("Handling interrupt(%d 0x%x): EIP %X    ESP %X", cpuState.intr, cpuState.intr, cpuState.eip, cpuState.esp);

				if (cpuState.intr < 32 && cpuState.intr != 1 && cpuState.intr != 3) {
					uint32_t _cr0, _cr2, _cr3;
					_asm {
						mov eax, cr0;
						mov _cr0, eax;

						mov eax, cr2;
						mov _cr2, eax;

						mov eax, cr3;
						mov _cr3, eax;
					}

					NeutronKernel::getInstance()->panic(cpuState.errorCode, "%s (0x%x)\n"
						"    EAX: %X    EBX: %X    ECX: %X    EDX: %X\n"
						"    ESI: %X    EDI: %X    EBP: %X EFLAGS: %X\n"
						"     CS: %X    EIP: %X     SS: %X    ESP: %X\n"
						"    CR0: %X    CR2: %X    CR3: %X",
						this->interruptManager()->exceptionName(cpuState.intr), cpuState.intr,
						cpuState.eax, cpuState.ebx, cpuState.ecx, cpuState.edx,
						cpuState.esi, cpuState.edi, cpuState.ebp, cpuState.eflags,
						cpuState.cs, cpuState.eip, cpuState.ss, cpuState.esp,
						_cr0, _cr2, _cr3);
				}

				if (cpuState.intr == 1) {
					/*kprintf("\nDEBUG  EAX: %X  EBX: %X  ECX: %X  EDX: %X\n"
						"ESI: %X  EDI: %X  EBP: %X EIP: %x  ESP: %x  ",
						cpuState.eax, cpuState.ebx, cpuState.ecx, cpuState.edx,
						cpuState.esi, cpuState.edi, cpuState.ebp, cpuState.eip, cpuState.esp);*/

					//kprintf("\nDEBUG  EIP: %x ESP: %x %X  ", cpuState.eip, cpuState.esp, *reinterpret_cast<uint32_t*>(cpuState.eip));

					//kprintf("\nDEBUG: EIP: %x ESP: %x", cpuState.eip, _sp, cpuState.esp);

					static uint32_t oldEIP = 0;
					LOG_TRACE("TRACE: ESP %X    EIP %X    %X %X", cpuState.esp, cpuState.eip, *reinterpret_cast<uint32_t*>(cpuState.eip), *reinterpret_cast<uint32_t*>(cpuState.eip + 4));
					if (MAX(cpuState.esp, oldEIP) - MIN(cpuState.esp, oldEIP) > 8) {
					}

					oldEIP = cpuState.eip;
					cpuState.eflags |= (1 << 8);
				}

				if (cpuState.intr >= 32 && cpuState.intr <= 47) { // IRQs
					LOG_INTERRUPT_TRACE("  Retrieve DriverManager instance");
					DriverManager* dm = DriverManager::getInstance();
					if (dm) {
						LOG_INTERRUPT_TRACE("    Invoke IRQ Listeners");
						dm->invokeIrqListeners(static_cast<uint8_t>(cpuState.intr - 32));
					}
				}

				if (cpuState.intr == 48) { // Syscall
					if (_syscallHandler) {
						LOG_INTERRUPT_TRACE("Invoking _syscallHandler: %X", _syscallHandler);
						cpuState.eax = this->_syscallHandler(cpuState.eax, reinterpret_cast<uintptr_t>(cpuState.ebx));
					}
				}

				if (cpuState.intr == 32) { // IRQ 0 (timer)
					_tickCount++;

					if (_timerHandler) {
						LOG_INTERRUPT_TRACE("Invoking _timerHandler: %X", _timerHandler);
						_timerHandler(_tickCount);
					}
				}

				LOG_INTERRUPT_TRACE("Returning from interrupt: EIP %X    ESP %X", cpuState.eip, cpuState.esp);

				return _currentState;
			}


			uint32_t ArchitectureX86::ramSize() {
				uint32_t maxEnd = 0;

				uint32_t procLen = 0;
				char* mmap = reinterpret_cast<char*>(mbinfo->mmap);
				while (procLen < mbinfo->mmapLength) {
					mb_mmap* e = reinterpret_cast<mb_mmap*>(mmap);

					if (e->type == 1) { // Free
						uint32_t start = static_cast<uint32_t>(e->address);
						uint32_t end = start + static_cast<uint32_t>(e->length);

						if (end > maxEnd) {
							maxEnd = end;
						}
					}

					procLen += e->structSize + 4;
					mmap += e->structSize + 4;
				}

				return maxEnd;
			}

			uint32_t ArchitectureX86::pageSize() {
				return 4096;
			}

			uint32_t ArchitectureX86::cpuStateSize() {
				return sizeof(CpuStateX86);
			}

			uint64_t ArchitectureX86::tickCount() {
				return this->_tickCount;
			}

			uint32_t ArchitectureX86::tickFrequency() {
				return TICK_FREQ;
			}

			void ArchitectureX86::delayMs(uint32_t ms) {
				//NeutronKernel::getInstance()->panic(0, "FIXME: ArchitectureX86::delayMs locks the system up!");
				// Calculate time to wait
				uint64_t endTicks = _tickCount + (ms * TICK_FREQ / 1000);

				//kprintf("Wait till %d\n", static_cast<uint32_t>(endTicks));
				// Wait
				while (_tickCount < endTicks) {
					_asm pause;
				}
			}

			Console* ArchitectureX86::console() {
				return &_console;
			}

			PhysicalMemoryManager* ArchitectureX86::pmm() {
				return &_pmm;
			}

			VirtualMemoryManager* ArchitectureX86::vmm() {
				return &_vmm;
			}

			VirtualMemoryContext* ArchitectureX86::kernelContext() {
				return _kernelContext;
			}

			HeapManager* ArchitectureX86::heap() {
				return &_heap;
			}


			void ArchitectureX86::preparePanic() {
				this->interruptManager()->disableInterrupts();
			}

			void ArchitectureX86::halt(bool completely) {
				if (completely) {
					_asm cli;
				}

				_asm {
				__halt_processor:
					hlt;
					jmp __halt_processor;
				}
			}


			Gdt* ArchitectureX86::gdt() {
				return &_gdt;
			}

			InterruptManager* ArchitectureX86::interruptManager() {
				return &_interruptManager;
			}

			void ArchitectureX86::atomicXchg(uint32_t* ptr1, uint32_t* ptr2) {
				uint32_t i1 = reinterpret_cast<uint32_t>(ptr1);
				uint32_t i2 = reinterpret_cast<uint32_t>(ptr2);
				_asm {
					// Copy the pointers into registers. The compiler will mess up otherwise
					mov eax, i1;
					mov ebx, i2;

					mov edx, DWORD PTR[eax];
					lock xchg DWORD PTR[ebx], edx;
					mov DWORD PTR[eax], edx;
				}
			}

#pragma region Port-IO
			void ArchitectureX86::outb(uint16_t port, uint8_t data) {
				_asm {
					mov dx, port;
					mov al, data;
					out dx, al;
				}
			}

			void ArchitectureX86::outw(uint16_t port, uint16_t data) {
				_asm {
					mov dx, port;
					mov ax, data;
					out dx, ax;
				}
			}

			void ArchitectureX86::outdw(uint16_t port, uint32_t data) {
				_asm {
					mov dx, port;
					mov eax, data;
					out dx, eax;
				}
			}


			uint8_t ArchitectureX86::inb(uint16_t port) {
				_asm {
					mov dx, port;
					in al, dx; // AL is the return value, no return statement needed.
				}
			}

			uint16_t ArchitectureX86::inw(uint16_t port) {
				_asm {
					mov dx, port;
					in ax, dx; // AX is the return value, no return statement needed.
				}
			}

			uint32_t ArchitectureX86::indw(uint16_t port) {
				_asm {
					mov dx, port;
					in eax, dx; // EAX is the return value, no return statement needed.
				}
			}
#pragma endregion
		}
	}
}

#endif // ARCH_X86
