#include <klibc/string.h>
#include "ConsoleX86.h"
#include "ArchitectureX86.h"

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			ConsoleX86::ConsoleX86() {
				this->window = reinterpret_cast<uint16_t*>(0xB8000);
				this->row = this->col = 0;
				this->attribute = 0x07;
				this->tabWidth = 4;
			}

			uint32_t ConsoleX86::height() {
				return CONSOLE_HEIGHT;
			}

			uint32_t ConsoleX86::width() {
				return CONSOLE_WIDTH;
			}

			void ConsoleX86::setForegroundColor(const ConsoleColor color) {
				this->attribute = static_cast<uint8_t>(this->attribute & 0xF0 | static_cast<uint8_t>(color));
			}

			void ConsoleX86::setBackgroundColor(const ConsoleColor color) {
				this->attribute = static_cast<uint8_t>(this->attribute & 0x0F | (static_cast<uint8_t>(color) << 4));
			}			

			void ConsoleX86::internal_putch(const char chr) {
				switch (chr) {
				case '\r':
				case '\n': {
					this->col = 0;
					this->row++;
					break;
				}

				case '\t': {
					int pad = this->tabWidth - (this->col + 1) % this->tabWidth;
					for (int i = 0; i < pad; i++) {
						this->internal_putch(' ');
					}
					break;
				}

				default: {
					this->window[this->row * 80 + this->col] = (chr | static_cast<uint16_t>(this->attribute << 8));
					col++;
					break;
				}
				}

				if (this->col >= CONSOLE_WIDTH) {
					this->col = 0;
					this->row++;
				}

				if (this->row >= CONSOLE_HEIGHT) {
					this->row = 24;
					// Move screen one line up, overwriting the first line.
					memcpy(this->window, this->window + CONSOLE_WIDTH, CONSOLE_WIDTH * (CONSOLE_HEIGHT - 1) * 2);

					// Clear the last line
					memsetw(this->window + CONSOLE_WIDTH * (CONSOLE_HEIGHT - 1), ' ' | this->attribute << 8, CONSOLE_WIDTH * 2);
				}
			}

			void ConsoleX86::internal_updateCursor(const bool force) {
				uint16_t pos;
				if (this->cursorEnabled) {
					pos = this->row * CONSOLE_WIDTH + this->col;
				}
				else if (force) {
					pos = 0x7d0; // Move the cursor out of the visible range.
				}
				else {
					return;
				}

				Architecture::currentArch()->outb(0x3D4, 14);
				Architecture::currentArch()->outb(0x3D5, static_cast<uint8_t>(pos >> 8));
				Architecture::currentArch()->outb(0x3D4, 15);
				Architecture::currentArch()->outb(0x3D5, static_cast<uint8_t>(pos));
			}
		}
	}
}

#endif // ARCH_X86
