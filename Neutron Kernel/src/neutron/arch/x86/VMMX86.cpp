#include <neutron.h>
#include "ArchitectureX86.h"
#include "MemoryContextX86.h"
#include "VMMX86.h"
#include "PMMX86.h"

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			void VMMX86::init(InitializationInfo* info) {

			}

			VirtualMemoryContext* VMMX86::createKernelContext() {
				MemoryContextX86* ctx = new MemoryContextX86(true);

				// Map whole memory one-to-one
				for (uint32_t i = 0; i < ((1024 * 1024) - 1); i++) {
					uintptr_t address = reinterpret_cast<uintptr_t>(i * PAGESIZE);
					ctx->map(address, address, VMM_FLAG_USER | VMM_FLAG_WRITE);
				}

				return ctx;
			}

			VirtualMemoryContext* VMMX86::createUserContext() {
				MemoryContextX86* ctx = new MemoryContextX86(true);

				// Map kernel memory as readable
				uint32_t kstartPage = KSTART_PAGE;
				uint32_t kendPage = KEND_PAGE;

				for (uint32_t i = kstartPage; i <= kendPage; i++) {
					uintptr_t address = reinterpret_cast<uintptr_t>(i * PAGESIZE);
					ctx->map(address, address, VMM_FLAG_USER);
				}

				return ctx;
			}

			void VMMX86::enable() {
				_asm {
					mov eax, cr0;
					or eax, (1 << 31);
					mov cr0, eax;
				}
			}
		}
	}
}

#endif // ARCH_X86
