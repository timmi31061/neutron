#pragma once
#include <neutron/arch/Console.h>

#ifdef ARCH_X86

#define CONSOLE_WIDTH 80
#define CONSOLE_HEIGHT 25

namespace Neutron {
	namespace Arch {
		namespace X86 {
			class ConsoleX86 : public Console
			{
			public:
				ConsoleX86();

				virtual uint32_t height();
				virtual uint32_t width();

				virtual void setForegroundColor(const ConsoleColor color);
				virtual void setBackgroundColor(const ConsoleColor color);

			protected:
				virtual void internal_putch(const char chr);
				virtual void internal_updateCursor(const bool force = false);

			private:
				// Pointer to Video-RAM
				uint16_t* window;
				uint8_t attribute;
			};
		}
	}
}

#endif // ARCH_X86
