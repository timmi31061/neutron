#pragma once
#include <neutron.h>
#include <neutron/arch/VirtualMemoryManager.h>

#ifdef ARCH_X86

#define VMM_FLAG_GLOBAL (1 << 8)
#define VMM_FLAG_USER (1 << 2)
#define VMM_FLAG_WRITE (1 << 1)
#define VMM_FLAG_DIRTY (1 << 6)

namespace Neutron {
	namespace Arch {
		namespace X86 {
			class VMMX86 : public VirtualMemoryManager
			{
			public:
				virtual void init(InitializationInfo* info);

				virtual VirtualMemoryContext* createKernelContext();
				virtual VirtualMemoryContext* createUserContext();
				virtual void enable();
			};
		}
	}
}

#endif // ARCH_X86
