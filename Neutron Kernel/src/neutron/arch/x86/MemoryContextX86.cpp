#include <neutron.h>
#include <neutron/exceptions/SystemException.h>
#include <neutron/exceptions/NotImplementedException.h>
#include "ArchitectureX86.h"
#include "MemoryContextX86.h"

using namespace Neutron::Exceptions;

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			MemoryContextX86::MemoryContextX86(bool isKernelContext)
				: VirtualMemoryContext(isKernelContext)
			{
				this->_pageDirectory = reinterpret_cast<PageDirectory*>(Architecture::currentArch()->pmm()->alloc(1, true));
			}

			void MemoryContextX86::appendPages(uintptr_t virtualAddress, uint32_t count, OnExistsAction onExists) {
				for (uint32_t i = 0; i < count; i++) {
					if (this->isPresent(virtualAddress)) {
						switch (onExists) {
						case OnExistsAction::Ignore:
							Architecture::currentArch()->pmm()->free(this->getPhysicalAddress(virtualAddress));
							break;

						case OnExistsAction::Throw:
							THROW(SystemException, "Page already mapped.");

						case OnExistsAction::Skip:
							continue;
						}
					}

					this->map(virtualAddress, Architecture::currentArch()->pmm()->alloc(1, true));
					virtualAddress = reinterpret_cast<uintptr_t>(reinterpret_cast<uint32_t>(virtualAddress) + Architecture::currentArch()->pageSize());
				}
			}

			void MemoryContextX86::map(uintptr_t virtualAddress, uintptr_t physicalAddress, uint16_t flags) {
				this->_pageDirectory->map(virtualAddress, physicalAddress, flags);
			}

			void MemoryContextX86::unmap(uintptr_t virtualAddress) {
				Architecture::currentArch()->pmm()->free(this->getPhysicalAddress(virtualAddress));
				this->_pageDirectory->unmap(virtualAddress);
			}
			
			uintptr_t MemoryContextX86::getPhysicalAddress(uintptr_t virtualAddress) {
				try {
					uint32_t dirIndex = reinterpret_cast<uint32_t>(virtualAddress) >> 22;
					uint32_t tblIndex = reinterpret_cast<uint32_t>(virtualAddress) >> 12 & 0x3ff;
					uint32_t pgOffset = reinterpret_cast<uint32_t>(virtualAddress) & 0xfff;

					return reinterpret_cast<uintptr_t>(reinterpret_cast<uint32_t>(this->_pageDirectory->getTable(dirIndex)->getEntry(tblIndex)->getAddress()) | pgOffset);
				}
				catch (...) {
					return NULL_PTR;
				}
			}

			uintptr_t MemoryContextX86::getVirtualAddress(uintptr_t pysicalAddress) {
				THROW_NOT_IMPLEMENTED();
			}

			bool MemoryContextX86::isPresent(uintptr_t virtualAddress) {
				uint32_t dirIndex = reinterpret_cast<uint32_t>(virtualAddress) >> 22;
				uint32_t tblIndex = reinterpret_cast<uint32_t>(virtualAddress) >> 12 & 0x3FF;
				if (this->_pageDirectory->getEntry(dirIndex, false)->present) {
					if (this->_pageDirectory->getTable(dirIndex)->getEntry(tblIndex, false)->present) {
						return true;
					}
				}

				return false;
			}

			void MemoryContextX86::enable() {
				uintptr_t dir = this->_pageDirectory;
				_asm {
					mov eax, dir;
					mov cr3, eax;
				}
			}

			uintptr_t MemoryContextX86::contextAddress() {
				return _pageDirectory;
			}
		}
	}
}

#endif // ARCH_X86
