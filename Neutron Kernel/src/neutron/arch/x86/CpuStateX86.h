#pragma once
#include <neutron.h>
#include <neutron/arch/CpuState.h>

#ifdef ARCH_X86

namespace Neutron {
	namespace Arch {
		namespace X86 {
			class CpuStateX86 : public CpuState
			{
			public: /* DO NOT ADD VARIABLES IF THEY AREN'T PUSHED/POPED BY InterruptManager::CommonInterruptHandler! You WILL break something. */
				uint16_t   gs;
				uint16_t   fs;
				uint16_t   es;
				uint16_t   ds;

				uint32_t   cr3;
				uint32_t   eax;
				uint32_t   ebx;
				uint32_t   ecx;
				uint32_t   edx;
				uint32_t   esi;
				uint32_t   edi;
				uint32_t   ebp;

				uint32_t   intr;
				uint32_t   errorCode;

				uint32_t   eip;
				uint32_t   cs;
				uint32_t   eflags;
				uint32_t   esp;
				uint32_t   ss;
			};
		}
	}
}

#endif // ARCH_X86
