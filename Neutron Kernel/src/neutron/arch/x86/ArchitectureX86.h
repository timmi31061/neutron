#pragma once
#include <neutron/arch/Architecture.h>
#include "facilities/InterruptManager.h"
#include "facilities/Gdt.h"
#include "CpuStateX86.h"
#include "MemoryContextX86.h"

#ifdef ARCH_X86

#define PAGESIZE 4096
#define TICK_FREQ 10

namespace Neutron {
	namespace Arch {
		namespace X86 {
			class ArchitectureX86 : public Architecture
			{
			public:
				ArchitectureX86();

				virtual bool init(InitializationInfo* info);
				virtual void registerSyscallHandler(SyscallHandler handler);
				virtual void registerTimerHandler(TimerHandler handler);

				virtual void setCpuState(CpuState* cpuState);
				virtual CpuState* cpuState();
				virtual void initializeThreadCpuState(CpuState* cpuState, uintptr_t entrypoint, uintptr_t stack, VirtualMemoryContext* context, bool kernelMode = false);

				virtual uint32_t ramSize();
				virtual uint32_t pageSize();
				virtual uint32_t cpuStateSize();
				virtual uint64_t tickCount();
				virtual uint32_t tickFrequency();

				virtual void delayMs(uint32_t ms);

				virtual Console* console();
				virtual PhysicalMemoryManager* pmm();
				virtual VirtualMemoryManager* vmm();
				virtual VirtualMemoryContext* kernelContext();
				virtual HeapManager* heap();

				virtual void preparePanic();
				virtual void halt(bool completely = false);

				virtual Gdt* gdt();
				virtual InterruptManager* interruptManager();

				virtual void atomicXchg(uint32_t* ptr1, uint32_t* ptr2);

				virtual void outb(uint16_t port, uint8_t data);
				virtual void outw(uint16_t port, uint16_t data);
				virtual void outdw(uint16_t port, uint32_t data);

				virtual uint8_t inb(uint16_t port);
				virtual uint16_t inw(uint16_t port);
				virtual uint32_t indw(uint16_t port);

			protected:
				uint64_t _tickCount = 0;
				MemoryContextX86* _kernelContext = 0;
				SyscallHandler _syscallHandler = 0;
				TimerHandler _timerHandler = 0;
				CpuStateX86* _currentState = 0;

				virtual CpuStateX86* interruptHandler(CpuStateX86& cpuState);

				friend CpuStateX86* interruptHandlerDelegator(CpuStateX86& cpuState);
			};
		}
	}
}

#endif // ARCH_X86
