#pragma once
#include <neutron/arch/PhysicalMemoryManager.h>

#ifdef ARCH_X86

#define PMM_ENTRIES 32768
#define PMM_ENTRYWIDTH 32
#define PMM_BUFLEN (PMM_ENTRIES * PMM_ENTRYWIDTH / 4)

namespace Neutron {
	namespace Arch {
		namespace X86{
			class PMMX86 : public PhysicalMemoryManager
			{
			public:
				virtual void init(InitializationInfo* info);

				virtual uint32_t usageKB();
				virtual bool isFree(uint32_t pageNo);

				virtual uintptr_t alloc(uint32_t pageCount = 1, bool initialize = false);
				virtual void free(uintptr_t page, uint32_t pageCount = 1);

			protected:
				virtual uintptr_t allocSingle();

			protected:
				uint32_t usedPages = 0;
				uint32_t bitmap[PMM_ENTRIES];
			};
		}
	}
}

#endif // ARCH_X86
