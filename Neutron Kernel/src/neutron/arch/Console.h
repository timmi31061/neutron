#pragma once
#include <neutron.h>
#include "InitializationInfo.h"

namespace Neutron {
	namespace Arch {
		enum class ConsoleColor : uint8_t {
			Black = 0x0,
			Blue = 0x1,
			Green = 0x2,
			Cyan = 0x3,
			Red = 0x4,
			Magenta = 0x5,
			Brown = 0x6,
			LightGray = 0x7,
			DarkGray = 0x8,
			LightBlue = 0x9,
			LightGreen = 0xA,
			LightCyan = 0xB,
			LightRed = 0xC,
			LightMagenta = 0xD,
			Yellow = 0xE,
			White = 0xF
		};

		struct BackgroundConsoleColor
		{
			inline BackgroundConsoleColor(ConsoleColor color) : color(color) { }
			ConsoleColor color;
		};

		class Console
		{
		public:
			virtual void init();

			virtual void clear();

			virtual void enableCursor();
			virtual void disableCursor();
			virtual bool isCursorEnabled();

			virtual void setForegroundColor(const ConsoleColor color) = 0;
			virtual void setBackgroundColor(const ConsoleColor color) = 0;

			virtual uint32_t height() = 0;
			virtual uint32_t width() = 0;

			virtual Console& operator<<(const char chr);
			virtual Console& operator<<(const char* str);

			virtual Console& operator<<(const uint8_t num);
			virtual Console& operator<<(const uint16_t num);
			virtual Console& operator<<(const uint32_t num);

			virtual Console& operator<<(const ConsoleColor color);
			virtual Console& operator<<(const BackgroundConsoleColor color);

			uint8_t tabWidth;

			// Position on screen
			uint8_t row, col;

		protected:
			virtual void internal_putch(const char chr) = 0;
			virtual void internal_updateCursor(const bool force = false) = 0;

			bool cursorEnabled;
		};
	}
}
