#include <klibc/string.h>
#include <klibc/itoa.h>
#include "Console.h"

namespace Neutron {
	namespace Arch {
		void Console::init() {
			this->clear();
			this->disableCursor();
		}

		void Console::clear() {
			this->row = this->col = 0;
			for (uint32_t i = 0; i < this->width() * this->height(); i++) {
				this->internal_putch(' ');
			}
			this->row = this->col = 0;
			this->internal_updateCursor();
		}

		void Console::enableCursor() {
			this->cursorEnabled = true;
			this->internal_updateCursor();
		}

		void Console::disableCursor() {
			this->cursorEnabled = false;
			this->internal_updateCursor(true);
		}

		bool Console::isCursorEnabled() {
			return this->cursorEnabled;
		}

		Console& Console::operator<<(const char chr) {
			this->internal_putch(chr);
			this->internal_updateCursor();
			return *this;
		}

		Console& Console::operator<<(const char* str) {
			// Align (left | center | right)
			if (str[0] == '\033') {
				str++;
				switch (*str++) {
				case 'l':
					this->col = 0;
					break;

				case 'c':
					this->col = static_cast<uint8_t>(this->width() / 2 - (strlen(str) - 2) / 2);
					break;

				case 'r':
					this->col = static_cast<uint8_t>(this->width() - (strlen(str) - 2));
					break;

				default:
					str--;
					str--;
					break;
				}
			}

			while (*str) {
				this->internal_putch(*str++);
			}
			this->internal_updateCursor();
			return *this;
		}

		Console& Console::operator<<(const uint8_t num) {
			*this << static_cast<uint32_t>(num);
			return *this;
		}

		Console& Console::operator<<(const uint16_t num) {
			*this << static_cast<uint32_t>(num);
			return *this;
		}

		Console& Console::operator<<(const uint32_t num) {
			char itoaBuf[21];
			itoa(itoaBuf, num, 10);
			*this << itoaBuf;
			return *this;
		}

		Console& Console::operator<<(const ConsoleColor color) {
			this->setForegroundColor(color);
			return *this;
		}

		Console& Console::operator<<(const BackgroundConsoleColor color) {
			this->setBackgroundColor(color.color);
			return *this;
		}
	}
}
