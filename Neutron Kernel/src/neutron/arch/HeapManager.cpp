#include <neutron.h>
#include <klibc/string.h>
#include <util/findSeq.h>
#include "Architecture.h"
#include "HeapManager.h"

#define SET_ALLOCATED(bitmap, index) (bitmap[(index) / 8] &= ~(1 << ((index) % 8)))
#define SET_FREE(bitmap, index) (bitmap[(index) / 8] |= (1 << ((index) % 8)))

namespace Neutron {
	namespace Arch {
		void HeapManager::init() {
			Architecture* arch = Architecture::currentArch();
			// Allocate page bitmap
			{
				// Calculate bitmap size in bytes and its page count to allocate
				uint32_t bmSize = arch->ramSize() / (arch->pageSize() * 8);
				uint32_t pages = CEILDIV(bmSize, arch->pageSize());

				// Allocate
				this->pageBitmap = reinterpret_cast<uint8_t*>(arch->pmm()->alloc(pages, true));
				this->pageBitmapSize = pages * arch->pageSize();			
			}

			// Allocate cluster bitmap
			{
				// Calculate bitmap size in bytes and its page count to allocate
				uint32_t bmSize = arch->ramSize() / (HEAP_CLUSTERSIZE * 8);
				uint32_t pages = CEILDIV(bmSize, arch->pageSize());

				// Calculate how much clusters a page contain
				this->clustersPerPage = CEILDIV(arch->pageSize(), HEAP_CLUSTERSIZE);
				this->clusterBitmapBytesPerPage = this->clustersPerPage / 8;

				// Allocate
				this->clusterBitmap = reinterpret_cast<uint8_t*>(arch->pmm()->alloc(pages, true));
				this->clusterBitmapSize = pages * arch->pageSize();
			}
		}

		uintptr_t HeapManager::alloc(size_t size) {
			// Blocks are preceded by a header
			uint32_t blockSize = size + sizeof(HeapBlockHeader);

			// Find block
			uint32_t blockIndex = findSeq(this->clusterBitmap, this->clusterBitmapSize, CEILDIV(blockSize, HEAP_CLUSTERSIZE));
			uint32_t blockAddress = blockIndex * HEAP_CLUSTERSIZE;

			// When it is 0 (zero) no fitting memory block is free
			if (blockAddress == 0) {
				// Try to allocate new pages
				if (this->allocPages(CEILDIV(blockSize, Architecture::currentArch()->pageSize()))) {
					// Memory is available!
					return this->alloc(size);
				}
				else {
					// No free memory
					return 0;
				}
			}

			// Set clusters as 'allocated'
			for (uint32_t i = 0; i < CEILDIV(blockSize, HEAP_CLUSTERSIZE); i++) {
				SET_ALLOCATED(this->clusterBitmap, i + blockIndex);
			}

			// Set header
			HeapBlockHeader* header = reinterpret_cast<HeapBlockHeader*>(blockAddress);
			header->sizeInClusters = CEILDIV(blockSize, HEAP_CLUSTERSIZE);


			// Return address
			return reinterpret_cast<uintptr_t>(blockAddress + sizeof(HeapBlockHeader));
		}

		void HeapManager::free(uintptr_t ptr) {
			// Get header
			HeapBlockHeader* header = reinterpret_cast<HeapBlockHeader*>(reinterpret_cast<uint32_t>(ptr) - 4);

			// Calculate start block index
			uint32_t blockIndex = reinterpret_cast<uint32_t>(ptr) / HEAP_CLUSTERSIZE;

			// Set block as 'free'
			for (uint32_t i = 0; i < header->sizeInClusters; i++) {
				SET_FREE(this->clusterBitmap, i + blockIndex);
			}
		}
		
		uintptr_t HeapManager::allocPages(uint32_t pageCount) {
			// Allocate pages
			uintptr_t block = Architecture::currentArch()->pmm()->alloc(pageCount, true);

			if (block == 0) {
				// No free memory
				return 0;
			}

			// Update bitmaps
			uint32_t startPage = reinterpret_cast<uint32_t>(block) / Architecture::currentArch()->pageSize();
			for (uint32_t i = 0; i < pageCount; i++) {
				SET_FREE(this->pageBitmap, startPage + i);

				for (uint32_t j = 0; j < this->clustersPerPage; j++) {
					SET_FREE(this->clusterBitmap, (startPage + i) * this->clustersPerPage + j);
				}
			}

			return block;
		}
	}
}
