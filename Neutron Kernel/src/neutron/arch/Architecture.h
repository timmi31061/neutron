#pragma once

#include <neutron.h>
#include "Console.h"
#include "PhysicalMemoryManager.h"
#include "VirtualMemoryManager.h"
#include "VirtualMemoryContext.h"
#include "HeapManager.h"
#include "CpuState.h"

namespace Neutron {
	namespace Arch {
		typedef uint32_t(*SyscallHandler)(uint32_t function, uintptr_t param);
		typedef void(*TimerHandler)(uint64_t tickCount);

		class NEUTRON_MODULE_API Architecture
		{
		public:
			virtual bool init(InitializationInfo* info) = 0;
			virtual void registerSyscallHandler(SyscallHandler handler) = 0;
			virtual void registerTimerHandler(TimerHandler handler) = 0;

			virtual void setCpuState(CpuState* cpuState) = 0;
			virtual CpuState* cpuState() = 0;
			virtual void initializeThreadCpuState(CpuState* cpuState, uintptr_t entrypoint, uintptr_t stack, VirtualMemoryContext* context, bool kernelMode = false) = 0;

			virtual uint32_t ramSize() = 0;
			virtual uint32_t pageSize() = 0;
			virtual uint32_t cpuStateSize() = 0;
			virtual uint64_t tickCount() = 0;
			virtual uint32_t tickFrequency() = 0;
			
			virtual void delayMs(uint32_t ms) = 0;

			virtual Console* console() = 0;
			virtual PhysicalMemoryManager* pmm() = 0;
			virtual VirtualMemoryManager* vmm() = 0;
			virtual VirtualMemoryContext* kernelContext() = 0;
			virtual HeapManager* heap() = 0;

			virtual void preparePanic() = 0;
			virtual void halt(bool completely = false) = 0;

			virtual void atomicXchg(uint32_t* ptr1, uint32_t* ptr2) = 0;

			virtual void outb(uint16_t port, uint8_t data) = 0;
			virtual void outw(uint16_t port, uint16_t data) = 0;
			virtual void outdw(uint16_t port, uint32_t data) = 0;

			virtual uint8_t inb(uint16_t port) = 0;
			virtual uint16_t inw(uint16_t port) = 0;
			virtual uint32_t indw(uint16_t port) = 0;

			static void setArch(Architecture* arch);
			static void enable(InitializationInfo* info);
			static Architecture* currentArch();

		private:
		};
	}
}
