#pragma once
#include <neutron.h>
#include <neutron/driver/base/StorageDriver.h>
#include <neutron/driver/DriverManager.h>

namespace Neutron {
	namespace Driver {
		namespace Arch {
			namespace X86 {
				namespace Storage {
					class AtaDriver : public Base::StorageDriver {
					public:
						virtual bool init(DriverManager* manager);

						virtual void beforeAccess();
						virtual void afterAccess();

						virtual void readBlocks(uintptr_t destination, uint64_t blockID, uint64_t blockCount);
						virtual void writeBlocks(uintptr_t source, uint64_t blockID, uint64_t blockCount);

						virtual uint32_t blockSize();

					protected:
						void accessLba(bool read, uintptr_t buffer, uint64_t blockID, uint64_t blockCount);
					};
				}
			}
		}
	}
}
