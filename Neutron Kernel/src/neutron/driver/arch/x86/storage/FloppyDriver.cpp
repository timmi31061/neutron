#include <neutron.h>
#include "FloppyDriver.h"
#include <neutron/arch/Architecture.h>
#include <neutron/driver/DriverManager.h>
#include <neutron/NeutronKernel.h>

#define DMA_PORT_PAGE 0x81
#define DMA_PORT_ADDR 0x04
#define DMA_PORT_COUNT 0x05
#define DMA_PORT_MASK 0x0A
//#define DMA_PORT_FLIPFLOP 0x0C
#define DMA_PORT_FLIPFLOP 0xD8
#define DMA_PORT_MODE 0x0B

#define DMA_MODE_READ 0x04
#define DMA_MODE_WRITE 0x08
#define DMA_MODE_AUTO 0x10
#define DMA_MODE_SINGLE 0x40

#define DMA_CLEAR_FLIPFLOP() arch->outb(DMA_PORT_FLIPFLOP, 0)

#define DMA_BUFFER ((uint32_t)0x200000)
#define DMA_BUFFER_PAGE ((DMA_BUFFER >> 16) & 0xFF)
#define DMA_BUFFER_OFFSET (DMA_BUFFER & 0xFFFF)


#define FDC_READY_OUT ((Architecture::currentArch()->inb(FDC_PORT_STATUS) & 0xc0) == 0x80)
#define FDC_READY_IN ((Architecture::currentArch()->inb(FDC_PORT_STATUS) & 0xc0) == 0xc0)

#define FDC_PORT_STATUS 0x3F4
#define FDC_PORT_DOR 0x3F2
#define FDC_PORT_DATA 0x3F5
#define FDC_PORT_CCR 0x3F7

#define FDC_DOR_RESET 0
#define FDC_DOR_ENABLE_CONTROLLER 0xC
#define FDC_DOR_STOP_MOTOR FDC_DOR_ENABLE_CONTROLLER
#define FDC_DOR_START_MOTOR (FDC_DOR_STOP_MOTOR | 0x10)

#define FLOPPY_SPINUP_DELAY 100
#define FLOPPY_SECTORS_PER_TRACK 18

using namespace Neutron;
using namespace Neutron::Arch;
using namespace Neutron::Multitasking;

namespace Neutron {
	namespace Driver {
		namespace Arch {
			namespace X86 {
				namespace Storage {

					bool FloppyDriver::init(DriverManager* manager) {
						_mre = new ManualResetEvent();

						manager->registerIrqListener(6, IRQ_LISTENER(FloppyDriver::irqHandler));

						LOG_DEBUG("[X86_FLOPPY] --- Init FDC");
						//configure();
						reset();
						beforeAccess();
						recalibrate();
						afterAccess();

						LOG_DEBUG("[X86_FLOPPY] --- Init done");

						return true;
					}

					void FloppyDriver::beforeAccess() {
						LOG_TRACE("[X86_FLOPPY] beforeAccess");

						// Start motor
						setMotor(true);
						NeutronKernel::getInstance()->delayMs(FLOPPY_SPINUP_DELAY);
					}

					void FloppyDriver::afterAccess() {
						LOG_TRACE("[X86_FLOPPY] afterAccess");

						// Stop motor
						setMotor(false);
					}

					void FloppyDriver::readBlocks(uintptr_t destination, uint64_t blockID, uint64_t blockCount) {
						LOG_TRACE("[X86_FLOPPY] readBlocks(%X, %d, %d)", destination, static_cast<uint32_t>(blockID), static_cast<uint32_t>(blockCount));

						if (destination == 0 || blockCount == 0) {
							return;
						}

						if (blockCount > 255) {
							blockCount = 255;
						}

						uint8_t head, cylinder, sector;
						lbaToChs(blockID, &head, &cylinder, &sector);

						transferData(head, cylinder, sector, static_cast<uint8_t>(blockCount), false);
						memcpy(destination, reinterpret_cast<uintptr_t>(DMA_BUFFER), 512);
					}

					void FloppyDriver::writeBlocks(uintptr_t source, uint64_t blockID, uint64_t blockCount) {
						LOG_TRACE("[X86_FLOPPY] writeBlocks(%X, %d, %d)", source, static_cast<uint32_t>(blockID), static_cast<uint32_t>(blockCount));

					}

					uint32_t FloppyDriver::blockSize() {
						return 512;
					}

					void FloppyDriver::irqHandler(uint8_t irq) {
						LOG_TRACE("[X86_FLOPPY] irqHandler");
						_mre->set();
					}

					void FloppyDriver::sendByte(uint8_t byte) {
						while (!FDC_READY_OUT);
						Architecture::currentArch()->outb(FDC_PORT_DATA, byte);
					}

					uint8_t FloppyDriver::readByte() {
						while (!FDC_READY_IN);
						return Architecture::currentArch()->inb(FDC_PORT_DATA);
					}

					void FloppyDriver::transferData(uint8_t head, uint8_t cylinder, uint8_t sector, uint8_t sectorCount, bool isWrite) {
						for (uint8_t i = 0; i < 3; i++) {
							if (i) {
								LOG_TRACE("[X86_FLOPPY] transferData retry");
							}
							else {
								LOG_TRACE("[X86_FLOPPY] transferData");
							}

							if (!FDC_READY_OUT) {
								reset();
							}

							// Reset first, to prevent race conditions
							_mre->reset();

							prepareFloppyTransfer(static_cast<uint16_t>(sectorCount * blockSize()), isWrite);

							if (isWrite) {
								sendByte(FloppyCommand::WriteData);
							}
							else {
								sendByte(FloppyCommand::ReadData);
							}

							sendByte(head << 2);
							sendByte(cylinder);
							sendByte(head);
							sendByte(sector);
							sendByte(2);
							sendByte(sectorCount);
							sendByte(0x1b);
							sendByte(0xff);

							// Wait for IRQ
							// ToDo: Timeout
							LOG_TRACE("[X86_FLOPPY] transferData: Wait for IRQ");
							_mre->waitOne();

							uint8_t st0 = readByte();
							uint8_t st1 = readByte();
							uint8_t st2 = readByte();
							uint8_t cylinder = readByte();
							uint8_t endingHead = readByte();
							uint8_t endingSector = readByte();
							readByte();

							if ((st0 & 0x10) == 0) {
								return;
							}
						}

						// ToDo: Error handling
						NeutronKernel::getInstance()->panic(0, "[X86_FLOPPY] Failed to transfer data");
					}

					void FloppyDriver::recalibrate() {
						for (uint8_t i = 0; i < 3; i++) {
							if (i) {
								LOG_TRACE("[X86_FLOPPY] recalibrate retry");
							}
							else {
								LOG_TRACE("[X86_FLOPPY] recalibrate");
							}

							if (!FDC_READY_OUT) {
								reset();
							}

							// Reset first, to prevent race conditions
							_mre->reset();

							sendByte(FloppyCommand::Recalibrate);
							sendByte(0);

							// Wait for IRQ
							// ToDo: Timeout
							LOG_TRACE("[X86_FLOPPY] recalibrate: Wait for IRQ");
							_mre->waitOne();

							uint8_t st0 = senseInterrupt();
							LOG_TRACE("[X86_FLOPPY] recalibrate: st0 is 0x%x", st0);
							if ((st0 & 0x10) == 0) {
								return;
							}
						}

						// ToDo: Error handling
						NeutronKernel::getInstance()->panic(0, "[X86_FLOPPY] Failed to recalibrate FDD");
					}

					void FloppyDriver::seek(uint8_t head, uint8_t cylinder) {
						for (uint8_t i = 0; i < 3; i++) {
							if (!FDC_READY_OUT) {
								reset();
							}

							// Reset first, to prevent race conditions
							_mre->reset();

							sendByte(FloppyCommand::Seek);
							sendByte(head << 2);
							sendByte(cylinder);

							// Wait for IRQ
							// ToDo: Timeout
							LOG_TRACE("[X86_FLOPPY] seek: Wait for IRQ");
							_mre->waitOne();

							if ((senseInterrupt() & 0x10) == 0) {
								return;
							}
						}

						// ToDo: Error handling
						NeutronKernel::getInstance()->panic(0, "[X86_FLOPPY] Failed to seek FDD");
					}

					uint8_t FloppyDriver::senseInterrupt() {
						LOG_TRACE("[X86_FLOPPY] senseInterrupt");

						if (!FDC_READY_OUT) {
							reset();
						}

						sendByte(FloppyCommand::SenseInterrupt);
						uint8_t st0 = readByte();
						readByte();

						return st0;
					}

					void FloppyDriver::configure() {
						LOG_TRACE("[X86_FLOPPY] configure");

						if (!FDC_READY_OUT) {
							reset();
						}

						sendByte(FloppyCommand::Configure);
						sendByte(0);
						//6sendByte(0x57); // Recommended settings
						sendByte(0x48); // Recommended settings
						sendByte(0);
					}

					void FloppyDriver::specify() {
						LOG_TRACE("[X86_FLOPPY] specify");

						if (!FDC_READY_OUT) {
							reset();
						}

						sendByte(FloppyCommand::Specify);
						sendByte(0);
						sendByte(0);
					}

					void FloppyDriver::reset() {
						LOG_TRACE("[X86_FLOPPY] reset");

						// Reset first, to prevent race conditions
						_mre->reset();

						Architecture::currentArch()->outb(FDC_PORT_DOR, FDC_DOR_RESET);
						//NeutronKernel::getInstance()->delayMs(10);
						Architecture::currentArch()->outb(FDC_PORT_DOR, FDC_DOR_ENABLE_CONTROLLER);

						// Wait for IRQ
						LOG_TRACE("[X86_FLOPPY] reset: Wait for IRQ");
						_mre->waitOne();

						senseInterrupt();
						senseInterrupt();
						senseInterrupt();
						senseInterrupt();

						Architecture::currentArch()->outb(FDC_PORT_CCR, 0);
						specify();
						configure();

						//Architecture::currentArch()->outb(FDC_PORT_DOR, FDC_DOR_ENABLE_CONTROLLER);
					}

					void FloppyDriver::setMotor(bool enabled) {
						if (enabled) {
							Architecture::currentArch()->outb(FDC_PORT_DOR, FDC_DOR_START_MOTOR);
						}
						else {
							Architecture::currentArch()->outb(FDC_PORT_DOR, FDC_DOR_STOP_MOTOR);
						}
					}

					void FloppyDriver::prepareFloppyTransfer(uint16_t byteCount, bool write) {
						/*
						 * NOTE: Writing to the same IO-Port twice is slow.
						 *       The performance will be increased by writing
						 *       to a different port before writing the second byte.
						 */

						byteCount -= 1;

						Architecture* arch = Architecture::currentArch();

						// Mask channel 2
						arch->outb(DMA_PORT_MASK, 0x06);
						
						// Send address
						DMA_CLEAR_FLIPFLOP();
						arch->outb(DMA_PORT_ADDR, DMA_BUFFER_OFFSET & 0xFF);
						arch->outb(DMA_PORT_ADDR, DMA_BUFFER_OFFSET >> 8);

						arch->outb(DMA_PORT_PAGE, DMA_BUFFER_PAGE);

						// Send count (low byte)
						DMA_CLEAR_FLIPFLOP();
						arch->outb(DMA_PORT_COUNT, byteCount & 0xFF);
						arch->outb(DMA_PORT_COUNT, byteCount >> 8);

						// Select mode
						uint8_t mode = 2 | DMA_MODE_SINGLE | DMA_MODE_AUTO;
						if (write) {
							mode |= DMA_MODE_WRITE;
						}
						else {
							mode |= DMA_MODE_READ;
						}
						arch->outb(DMA_PORT_MODE, mode);

						// Send count (high byte)

						// Unmask channel 2
						arch->outb(DMA_PORT_MASK, 0x02);
					}

					void FloppyDriver::lbaToChs(uint64_t lba, uint8_t* head, uint8_t* cylinder, uint8_t* sector) {
						*cylinder = lba / (FLOPPY_SECTORS_PER_TRACK * 2);
						*head = (lba % (FLOPPY_SECTORS_PER_TRACK * 2)) / (FLOPPY_SECTORS_PER_TRACK);
						//*sector = lba % FLOPPY_SECTORS_PER_TRACK + 1;
						*sector = ((lba % (FLOPPY_SECTORS_PER_TRACK * 2)) % FLOPPY_SECTORS_PER_TRACK) + 1;
					}
				}
			}
		}
	}
}
