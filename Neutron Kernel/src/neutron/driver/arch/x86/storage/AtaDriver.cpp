#include <neutron.h>
#include <neutron/arch/Architecture.h>
#include "AtaDriver.h"

#include <kiostream.h>

using namespace Neutron::Arch; 

#define PORT_BLOCK_COUNT 0x1F2
#define PORT_CMD 0x1F7
#define PORT_DATA 0x1F0
#define PORT_LBA_0_7 0x1F3
#define PORT_LBA_8_15 0x1F4
#define PORT_LBA_16_23 0x1F5
#define PORT_LBA_24_27 0x1F6


namespace Neutron {
	namespace Driver {
		namespace Arch {
			namespace X86 {
				namespace Storage {
					bool AtaDriver::init(DriverManager* manager) {
						return true;
					}

					void AtaDriver::beforeAccess() {
						// Empty
					}

					void AtaDriver::afterAccess() {
						// Empty
					}

					void AtaDriver::readBlocks(uintptr_t destination, uint64_t blockID, uint64_t blockCount) {
						this->accessLba(true, destination, blockID, blockCount);
					}

					void AtaDriver::writeBlocks(uintptr_t source, uint64_t blockID, uint64_t blockCount) {
						this->accessLba(false, source, blockID, blockCount);
					}

					uint32_t AtaDriver::blockSize() {
						return 512;
					}

					void AtaDriver::accessLba(bool read, uintptr_t buffer, uint64_t blockID, uint64_t blockCount) {
						// FIXME: max 128 GiB accessable

						// Send bits 24 - 27 of LBA and LBA mode selector
						Architecture::currentArch()->outb(PORT_LBA_24_27, static_cast<uint8_t>((blockID >> 24) | 0xE0));

						// Send sector count
						Architecture::currentArch()->outb(PORT_BLOCK_COUNT, static_cast<uint8_t>(blockCount));

						// Send bits 0 - 7 of LBA
						Architecture::currentArch()->outb(PORT_LBA_0_7, static_cast<uint8_t>(blockID));

						// Send bits 8 - 15 of LBA
						Architecture::currentArch()->outb(PORT_LBA_8_15, static_cast<uint8_t>(blockID >> 8));

						// Send bits 16 - 23 of LBA
						Architecture::currentArch()->outb(PORT_LBA_16_23, static_cast<uint8_t>(blockID >> 16));


						// Wait, until the HDD is ready
						// "The suggestion is to read the Status register FIVE TIMES,
						// and only pay attention to the value returned by the last one"
						// http://wiki.osdev.org/ATA_PIO_Mode#400ns_delays
						Architecture::currentArch()->inb(PORT_CMD);
						Architecture::currentArch()->inb(PORT_CMD);
						Architecture::currentArch()->inb(PORT_CMD);
						Architecture::currentArch()->inb(PORT_CMD);
						while ((Architecture::currentArch()->inb(PORT_CMD) & 0xC0) != 0x40);

						uint32_t reps = 256 * static_cast<uint32_t>(blockCount);
						if (read) {
							// Send read command
							Architecture::currentArch()->outb(PORT_CMD, 0x20);

							// Wait, until the HDD is ready
							while ((Architecture::currentArch()->inb(PORT_CMD) & 8) == 0);

							_asm {
								cld; // increasing memory adresses
								mov dx, PORT_DATA;
								mov ecx, reps;
								mov edi, buffer;

								// read data
								rep insw;
							}
						}
						else {
							// Send write command
							Architecture::currentArch()->outb(PORT_CMD, 0x30);

							// Wait, until the HDD is ready
							while ((Architecture::currentArch()->inb(PORT_CMD) & 8) == 0);

							_asm {
								cld; // increasing memory adresses
								mov dx, PORT_DATA;
								mov ecx, reps;
								mov esi, buffer;

								// write data
							write:
								outsw;
								nop;
								nop;
								jmp write;
							}
						}
					}
				}
			}
		}
	}
}
