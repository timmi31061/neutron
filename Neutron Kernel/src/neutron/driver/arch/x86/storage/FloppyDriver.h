#pragma once
#include <neutron.h>
#include <neutron/driver/base/StorageDriver.h>
#include <neutron/multitasking/ManualResetEvent.h>

namespace Neutron {
	namespace Driver {
		namespace Arch {
			namespace X86 {
				namespace Storage {
					enum FloppyCommand : uint8_t {
						ReadTrack = 0x02,

						/**
							Specify drive info.
							Recommended parameter values:
							 - SRT, HUT, HLT: 0 (very safe, but a bit slower seek actions)
							 - NDMA: 0, if DMA is used
							No IRQ.
							Two parameter bytes:
							 - First: (SRT << 4) | HUT
							 - Second: (HLT << 1) | NDMA
							No result bytes.
						*/
						Specify = 0x03,

						SenseDriveStatus = 0x04,

						/**
							Write data.
							IRQ.
							Eight parameter bytes:
							 - First: (head number << 2) | drive number
							 - Second: cylinder number
							 - Third: head number (repetition of the above value)
							 - Fourth: starting sector number (one-based)
							 - Fifth: 2
							 - Sixth: sector count
							 - Seventh: 0x1b
							 - Eighth: 0xff
							Seven result bytes:
							 - First: st0
							 - Second: st1
							 - Third: st2
							 - Fourth: cylinder number
							 - Fifth: ending head number
							 - Sixth: ending sector number
							 - Seventh: 2
						*/
						WriteData = 0xc5,

						/**
							Read data.
							IRQ.
							Eight parameter bytes:
							- First: (head number << 2) | drive number
							- Second: cylinder number
							- Third: head number (repetition of the above value)
							- Fourth: starting sector number (one-based)
							- Fifth: 2
							- Sixth: sector count
							- Seventh: 0x1b
							- Eighth: 0xff
							Seven result bytes:
							- First: st0
							- Second: st1
							- Third: st2
							- Fourth: cylinder number
							- Fifth: ending head number
							- Sixth: ending sector number
							- Seventh: 2
						*/
						ReadData = 0xc6,

						/**
							Move head to cylinder 0. Motor needs to be on.
							IRQ may take up to three seconds to arrive.
							One parameter byte: drive number (0 to 3)
							No result bytes.
						*/
						Recalibrate = 0x07,

						/**
							Acknowledge IRQ. Get error code.
							Only call this on IRQ produced by:
							 - A Controller Reset with enabled Drive Polling mode
							 - Seek or relative seek
							 - Recalibrate
							In any other case this will lock up the controller until it is resetted.
							No IRQ.
							No parameters.
							Two result bytes:
							 - First: st0
							 - Second: Controller's idea of the current cylinder (WTF? The controller is guessing?)
						*/
						SenseInterrupt = 0x08,

						WriteDeletedData = 0x09,
						ReadID = 0x0a,
						ReadDeletedData = 0x0c,	
						FormatTrack = 0x0d,

						/**
							Seek the heads.
							The motor must be on, and the drive has to be selected.
							IRQ may take up to three seconds to arrive.
							Two parameter bytes:
							 - First: (head number << 2) | drive number
							 - Second: cylinder number
							No result bytes.
						*/
						Seek = 0x0f,

						/**
							Read version.
							No IRQ. No parameters.
							Returns one byte (0x90 means controller is a 82077AA).
						*/
						Version = 0x10,

						ScanEqual = 0x11,
						PerpendicularMode = 0x12,

						/**
							Initialize controller settings. See also Lock.
							A good setting is: Implied seek on, FIFO on, drive polling mode off, threshold = 8, precompensation = 0.
							No IRQ.
							Three parameter bytes:
							 - First: 0
							 - Second: (implied seek ENable << 6) | (fifo DISable << 5) | (drive polling mode DISable << 4) | (threshold - 1)
							 - Third: precompensation
							No result bytes.
						*/
						Configure = 0x13,

						/**
							Prevents a controller reset from deleting configured settings.
							No IRQ. No parameters.
							Returns one byte: lockBit << 4
						*/
						Lock = 0x94,

						/**
							Allows a controller reset deleting configured settings.
							No IRQ. No parameters.
							Returns one byte: lockBit << 4
						*/
						Unlock = 0x14,

						Verify = 22,
						ScanLowOrEqual = 25,
						ScanHighOrEqual = 29
					};

					class FloppyDriver : public Neutron::Driver::Base::StorageDriver {
					public:
						virtual bool init(DriverManager* manager);
						virtual void beforeAccess();
						virtual void afterAccess();

						virtual void readBlocks(uintptr_t destination, uint64_t blockID, uint64_t blockCount);
						virtual void writeBlocks(uintptr_t source, uint64_t blockID, uint64_t blockCount);

						virtual uint32_t blockSize();

					protected:
						bool _firstReset = true;
						Multitasking::ManualResetEvent* _mre;

						void irqHandler(uint8_t irq);

						void sendByte(uint8_t byte);
						uint8_t readByte();

						void transferData(uint8_t head, uint8_t cylinder, uint8_t sector, uint8_t sectorCount, bool isWrite);
						void recalibrate();
						void seek(uint8_t head, uint8_t cylinder);
						uint8_t senseInterrupt();
						void configure();
						void specify();
						void reset();
						void setMotor(bool enabled);
						void prepareFloppyTransfer(uint16_t byteCount, bool write);

						void lbaToChs(uint64_t lba, uint8_t* head, uint8_t* cylinder, uint8_t* sector);
					};
				}
			}
		}
	}
}
