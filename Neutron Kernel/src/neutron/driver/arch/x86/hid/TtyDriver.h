#pragma once
#include <neutron.h>
#include <neutron/driver/base/TtyDriver.h>
#include <neutron/driver/DriverManager.h>

namespace Neutron {
	namespace Driver {
		namespace Arch {
			namespace X86 {
				namespace HID {
					class TtyDriver : public Base::TtyDriver {
					public:
						virtual bool init(DriverManager* manager);
						void keycodeHandler(bool isBreak, uint8_t keycode);

					protected:
						void flushKbcBuffer();
						void sendCommand(uint8_t command);
						void irqHandler(uint8_t irq);

						bool shift, control, alt, capslock, capslockReleased, ralt;
					};
				}
			}
		}
	}
}
