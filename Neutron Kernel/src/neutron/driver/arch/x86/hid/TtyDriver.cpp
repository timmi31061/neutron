#include <neutron.h>
#include <neutron/arch/x86/ArchitectureX86.h>
#include "TtyDriver.h"
#include <neutron/driver/keycodes.h>
#include <neutron/driver/arch/x86/storage/FloppyDriver.h>
#include <neutron/multitasking/ManualResetEvent.h>
#include <util/dumpMem.h>

#include <kiostream.h>

using namespace Neutron::Arch::X86;

namespace Neutron {
	extern Multitasking::ManualResetEvent* mre;
	extern uint32_t block;

	namespace Driver {
		namespace Arch {
			namespace X86 {
				namespace HID {
#pragma warning(push)
#pragma warning(disable: 4245) // signed/unsigned mismatch
#pragma warning(disable: 4838) // conversion from 'type_1' to 'type_2' requires a narrowing conversion
					static const uint8_t scToKc[256] = {
						0, VK_ESCAPE,
						'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', VK_OEM_8, VK_OEM_9, VK_BACK,
						VK_TAB, 'Q', 'W', 'E', 'R', 'T', 'Z', 'U', 'I', 'O', 'P', VK_OEM_4, VK_PLUS, VK_RETURN, VK_LCONTROL,
						'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', VK_OEM_1, VK_OEM_7, VK_OEM_3, VK_LSHIFT, VK_OEM_12,
						'Y', 'X', 'C', 'V', 'B', 'N', 'M', VK_COMMA, VK_PERIOD, VK_MINUS, VK_RSHIFT,
						VK_MULTIPLY, VK_LALT, VK_SPACE, VK_CAPSLOCK,
						VK_F1, VK_F2, VK_F3, VK_F4, VK_F5, VK_F6, VK_F7, VK_F8, VK_F9, VK_F10, VK_SCROLL, VK_NUMLOCK,
						VK_NUMPAD7, VK_NUMPAD8, VK_NUMPAD9, VK_SUBTRACT,
						VK_NUMPAD4, VK_NUMPAD5, VK_NUMPAD6, VK_ADD,
						VK_NUMPAD1, VK_NUMPAD2, VK_NUMPAD3, VK_NUMPAD0,
						VK_DECIMAL, 0, 0, VK_OEM_5, VK_F11, VK_F12
					};

					static const uint8_t kcToText[256] = {
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '\n', 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ' ',
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0',
						'1', '2', '3', '4', '5', '6', '7', '8', '9', 0, 0, 0, 0, 0, 0, 0,
						'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
						'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 0, 0, 0, 0, 0, '0',
						'1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '+', '-', '/', ',', 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '+', ',', '-', '.', 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, '�', 0, '^', '�', '<', 0, '�', '�', '�',
						0, 0, '#'
					};

					static const uint8_t kcToText_shift[256] = {
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '=',
						'!', '"', '�', '$', '%', '&', '/', '(', ')', 0, 0, 0, 0, 0, 0, 0,
						'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
						'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '*', ';', '_', ':', 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, '�', 0, '�', '�', '>', 0, '�', '?', '`',
						0, 0, '\''
					};

					static const uint8_t kcToText_alt[256] = {
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '}',
						0, '�', '�', 0, 0, 0, '{', '[', ']', 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, '�', 0, 0, 0, 0, 0, 0, 0, '�', 0, 0, 0,
						'@', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '~', 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '|', 0, 0, '\\', 0,
						0, 0
					};
#pragma warning(pop)

					bool TtyDriver::init(DriverManager* manager) {
						manager->registerIrqListener(1, IRQ_LISTENER(TtyDriver::irqHandler));

						// Initialize keyboard
						this->flushKbcBuffer();
						this->sendCommand(0xf4);

						capslockReleased = true;

						return true;
					}

					void TtyDriver::irqHandler(uint8_t irq) {
						uint8_t scancode = Architecture::currentArch()->inb(0x60);

						bool isBreakCode = false;

						static bool isExt0 = false;
						static bool isExt1 = false;
						static uint8_t extByte = 0;
						static uint16_t extCode = 0;

						// Breakcode?
						if ((scancode & 0x80) && ((scancode != 0xE0) || isExt0) && ((scancode != 0xE1) || isExt1)) {
							isBreakCode = true;
							scancode &= ~0x80;
						}

						// Extended 0 begin?
						if (scancode == 0xE0) {
							isExt0 = true;
						}

						// Extended 1 begin?
						else if (scancode == 0xE1) {
							isExt1 = true;
							extCode = 0;
							extByte = 0;
						}

						// Extended 0 code byte?
						else if (isExt0) {
							// Drop fake shifts 
							if (scancode == 0x2A || scancode == 0x36) {
								isExt0 = false;
								return;
							}

							// E0-codes are chosen in a way that something meaningful
							// happens, when the driver just discards the 0xE0-byte.
							// So transform the scancode like an ordinary one.
							keycodeHandler(isBreakCode, scToKc[scancode]);
							return;
						}

						// Extended 1 code byte?
						else if (isExt1) {
							if (extByte == 0) {
								extCode = scancode;
								extByte++;
							}
							else  if (extByte == 1) {
								extCode |= static_cast<uint16_t>(scancode) << 8;
								isExt1 = false;

								// TODO: suppurt E1-codes
								return;
							}
						}

						// Ordinary scancode
						else {
							keycodeHandler(isBreakCode, scToKc[scancode]);
						}

						//kprintf("[DRV_X86_TTY] Scancode: %c\n", scToKc[scancode]);
						//kprintf("[DRV_X86_TTY] Scancode: %x %d\n", scancode, scancode);
					}

					void TtyDriver::keycodeHandler(bool isBreak, uint8_t keycode) {
						if (ralt) {
							control = true;
							alt = true;
						}

						if (capslock) {
							shift = true;
						}

						switch (keycode) {
						case VK_LSHIFT:
						case VK_RSHIFT:
							shift = !isBreak;
							break;

						case VK_LCONTROL:
						case VK_RCONTROL:
							control = !isBreak;
							break;

						case VK_LALT:
							alt = !isBreak;
							break;

						case VK_RALT:
							ralt = !isBreak;
							break;

						case VK_CAPSLOCK:
							if (capslockReleased) {
								capslock = !capslock;
								shift = capslock;
								capslockReleased = false;
							}

							if (isBreak) {
								capslockReleased = true;
							}
							break;

						default:
							if (!isBreak) {
								uint8_t chr = 0;

								if (!shift && !alt && !control) {
									chr = kcToText[keycode];
								}
								else if (shift && !alt && !control) {
									chr = kcToText_shift[keycode];
								}
								else if (!shift && alt && control) {
									chr = kcToText_alt[keycode];
								}

								if (chr != 0) {
									kprintf("%c", chr);
								}
							}
						}


						if (isBreak == false) {
							switch (keycode) {
							case VK_NUMPAD4:
								block--;
								break;

							case VK_NUMPAD6:
								block++;
								break;

							case VK_NUMPAD2:
								block -= 10;
								break;

							case VK_NUMPAD8:
								block += 10;
								break;

							default:
								return;
							}

							mre->set();
						}

						/*
						if (isBreak && keycode == VK_LCONTROL) {
							static bool onoff = false;
							onoff = !onoff;
							if (onoff) {
								kprintf("ON!\n");
								DriverManager::getInstance()->getDriver<Neutron::Driver::Arch::X86::Storage::FloppyDriver>(false)->beforeAccess();
							}
							else {
								kprintf("OFF!\n");
								DriverManager::getInstance()->getDriver<Neutron::Driver::Arch::X86::Storage::FloppyDriver>(false)->afterAccess();
							}
						}*/
					}

					void TtyDriver::flushKbcBuffer() {
						while (Architecture::currentArch()->inb(0x64) & 0x1) {
							Architecture::currentArch()->inb(0x60);
						}
					}

					void TtyDriver::sendCommand(uint8_t command) {
						// Wait, until the keyboard is ready
						while (Architecture::currentArch()->inb(0x64) & 0x2);

						Architecture::currentArch()->outb(0x60, command);
					}
				}
			}
		}
	}
}
