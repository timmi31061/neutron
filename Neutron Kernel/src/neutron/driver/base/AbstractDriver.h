#pragma once
#include <neutron.h>

//using namespace Neutron::Driver;

namespace Neutron {
	namespace Driver {
		class DriverManager;

		namespace Base {
			class AbstractDriver
			{
			public:
				virtual bool init(DriverManager* manager) = 0;
			};
		}
	}
}
