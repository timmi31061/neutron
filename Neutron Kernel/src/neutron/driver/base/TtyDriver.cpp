#include <neutron.h>
#include <neutron/arch/Architecture.h>
#include "TtyDriver.h"

namespace Neutron {
	namespace Driver {
		namespace Base {
			Console* TtyDriver::console() {
				return Architecture::currentArch()->console();
			}

			void TtyDriver::keyEvent(uint8_t keyCode) {

			}
		}
	}
}
