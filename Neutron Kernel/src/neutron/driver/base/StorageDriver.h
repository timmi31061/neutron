#pragma once
#include <neutron.h>
#include "AbstractDriver.h"

namespace Neutron {
	namespace Driver {
		namespace Base {
			class StorageDriver : public AbstractDriver
			{
			public:
				virtual void beforeAccess() = 0;
				virtual void afterAccess() = 0;

				virtual void readBlocks(uintptr_t destination, uint64_t blockID, uint64_t blockCount) = 0;
				virtual void writeBlocks(uintptr_t source, uint64_t blockID, uint64_t blockCount) = 0;

				virtual uint32_t blockSize() = 0;
			};
		}
	}
}
