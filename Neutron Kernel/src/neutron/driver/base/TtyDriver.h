#pragma once
#include <neutron.h>
#include <neutron/arch/Console.h>
#include "AbstractDriver.h"

using namespace Neutron::Arch;

namespace Neutron {
	namespace Driver {
		namespace Base {
			class TtyDriver : public AbstractDriver {
			public:
				virtual Console* console();

			protected:
				void keyEvent(uint8_t keyCode);
			};
		}
	}
}
