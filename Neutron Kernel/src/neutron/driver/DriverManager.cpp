#include <neutron.h>
#include <neutron/exceptions/SystemException.h>
#include <neutron/exceptions/OutOfRangeException.h>
#include "DriverManager.h"

#include <klibc/kprintf.h>

using namespace kstl;
using namespace Neutron::Exceptions;
using namespace Neutron::Driver::Base;

namespace Neutron {
	namespace Driver {
		static DriverManager* driverManager = 0;
		DriverManager* DriverManager::getInstance() {
			return driverManager;
		}

		DriverManager::DriverManager(uint8_t irqCount) {
			if (driverManager) {
				THROW(SystemException, "Tried to create multiple DriverManager");
			}

			this->_irqCount = irqCount;
			this->_irqListeners = new ArrayList<IrqListener*>*[irqCount];
			for (uint8_t i = 0; i < irqCount; i++) {
				this->_irqListeners[i] = new ArrayList<IrqListener*>();
			}

			driverManager = this;
		}

		DriverManager::~DriverManager() {
			// Delete irq listener list
			for (int16_t i = this->_irqCount - 1; i >= 0; i--) {
				delete this->_irqListeners[i];
			}
			delete this->_irqListeners;

			// Unload every driver
			while (_drivers.length()) {
				uint32_t i = _drivers.length() - 1;
				delete _drivers.valueAt(i);
				_drivers.removeAt(i);
			}
		}

		void DriverManager::registerIrqListener(uint8_t irq, AbstractDriver* driver, IrqListenerPtr listener) {
			if (irq >= _irqCount) {
				THROW_OUT_OF_RANGE();
			}

			IrqListener* l = new IrqListener();
			l->function = listener;
			l->instance = driver;
			this->_irqListeners[irq]->add(l);
		}

		void DriverManager::invokeIrqListeners(uint8_t irq) {
			if (irq >= _irqCount) {
				THROW_OUT_OF_RANGE();
			}

			ArrayList<IrqListener*>* listeners = this->_irqListeners[irq];
			for (uint32_t i = 0; i < listeners->length(); i++) {
				IrqListener* l = (*listeners)[i];
				(l->instance->*(l->function))(irq);
			}
		}
	}
}
