#pragma once
#include <neutron.h>
#include <kstl/list.h>
#include <kstl/map.h>
#include <klibc/typeinfo.h>
#include <neutron/exceptions/SystemException.h>
#include <neutron/driver/base/AbstractDriver.h>


//typedef void(*IrqListener)(uint8_t irq);

typedef void(Neutron::Driver::Base::AbstractDriver::*IrqListenerPtr)(uint8_t irq);
#define IRQ_LISTENER(listener) this, static_cast<IrqListenerPtr>(&listener)

namespace Neutron {
	namespace Driver {
		class NEUTRON_MODULE_API DriverManager {
		public:
			DriverManager(uint8_t irqCount);
			~DriverManager();

			static DriverManager* getInstance();

			void registerIrqListener(uint8_t irq, Neutron::Driver::Base::AbstractDriver* driver, IrqListenerPtr listener);
			void invokeIrqListeners(uint8_t irq);

			template <typename T>
			T* getDriver(bool loadIfNotPresent = true) {
				try{
					return reinterpret_cast<T*>(this->_drivers[reinterpret_cast<uint32_t>(&typeid(T))]);
				}
				catch (...) {
					if (loadIfNotPresent) {
						return loadDriver<T>();
					}
					else {
						THROW(Exceptions::SystemException, "Driver not present!");
					}
				}
			}
			
			template <typename T>
			T* loadDriver() {
				// Allocate driver
				Neutron::Driver::Base::AbstractDriver* drv = new T();
				_drivers.add(reinterpret_cast<uint32_t>(&typeid(T)), drv);

				// Initialize driver
				drv->init(this);
				return static_cast<T*>(drv);
			}

		private:
			struct IrqListener {
				IrqListenerPtr function;
				Neutron::Driver::Base::AbstractDriver* instance;
			};

			bool _initialized = false;
			uint8_t _irqCount;

			kstl::LinearMap<uint32_t, Neutron::Driver::Base::AbstractDriver*> _drivers;
			kstl::ArrayList<IrqListener*>** _irqListeners;
		};
	}
}
