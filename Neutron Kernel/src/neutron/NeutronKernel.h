#pragma once
#include <neutron.h>
#include <neutron/multitasking/Scheduler.h>
#include <neutron/logging/ILogger.h>

namespace Neutron {
	class NEUTRON_MODULE_API NeutronKernel final
	{
	public:
		NeutronKernel();

		void init();
		void enableMultitasking();
		void panic(uint32_t code, const char* format, ...);
		uint64_t tickCount();

		void delayMs(uint32_t ms);

		Multitasking::Scheduler* scheduler();

		Logging::ILogger* getLogger();
		void setLogger(Logging::ILogger* logger);

		static NeutronKernel* getInstance();

	private:
		bool _multitaskingEnabled = false;
		Multitasking::Scheduler* _scheduler;
		uint64_t _ticks = 0;
		Logging::ILogger* _logger;

		uint32_t syscall(uint32_t function, uintptr_t param);
		void timerTick(uint64_t ticks);

		friend uint32_t syscallDelegator(uint32_t function, uintptr_t param);
		friend void timerTickDelegator(uint64_t ticks);
	};
}
