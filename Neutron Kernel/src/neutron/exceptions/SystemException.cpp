#include <neutron.h>
#include "SystemException.h"

namespace Neutron {
	namespace Exceptions {
		SystemException::SystemException(const char* message) {
			_message = message;
		}

		const char* SystemException::message() const {
			return _message;
		}
	}
}
