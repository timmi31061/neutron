#pragma once
#include <neutron.h>

#ifdef DEBUG
#define THROW(exception, message) throw new exception(__FILE__ " (line " __LINE_STR__ "): " message)
#else
#define THROW(exception, message) throw new exception(__FUNCSIG__ ": " message)
#endif

namespace Neutron {
	namespace Exceptions {
		class NEUTRON_MODULE_API SystemException {
		public:
			SystemException(const char* message);
			const char* message() const;

		protected:
			const char* _message;
		};
	}
}
