#pragma once
#include <neutron.h>
#include "SystemException.h"

#define THROW_NOT_IMPLEMENTED() THROW(Neutron::Exceptions::NotImplementedException, "Not implemented!")
#define THROW_NOT_IMPLEMENTED_MSG(message) THROW(Neutron::Exceptions::NotImplementedException, message)

namespace Neutron {
	namespace Exceptions {
		class NEUTRON_MODULE_API NotImplementedException : public SystemException {
		public:
			NotImplementedException(const char* message);
		};
	}
}
