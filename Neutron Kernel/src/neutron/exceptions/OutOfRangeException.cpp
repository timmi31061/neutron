#include <neutron.h>
#include "OutOfRangeException.h"

namespace Neutron {
	namespace Exceptions {
		OutOfRangeException::OutOfRangeException(const char* message) : SystemException(message) {
		}
	}
}
