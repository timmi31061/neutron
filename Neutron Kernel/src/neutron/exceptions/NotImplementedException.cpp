#include <neutron.h>
#include <klibc/string.h>
#include "NotImplementedException.h"

namespace Neutron {
	namespace Exceptions {
		NotImplementedException::NotImplementedException(const char* message) : SystemException(message) {
		}
	}
}
