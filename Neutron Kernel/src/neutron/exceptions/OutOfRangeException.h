#pragma once
#include <neutron.h>
#include "SystemException.h"

#define THROW_OUT_OF_RANGE() THROW(Neutron::Exceptions::OutOfRangeException, "The parameter was out of range.")

namespace Neutron {
	namespace Exceptions {
		class NEUTRON_MODULE_API OutOfRangeException : public SystemException {
		public:
			OutOfRangeException(const char* message);
		};
	}
}
