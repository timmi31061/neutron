#pragma once
#include <neutron.h>
#include "AbstractLogger.h"

namespace Neutron {
	namespace Logging {
		class SerialLogger final : public AbstractLogger {
		public:
			SerialLogger();
			virtual void internalLog(const char* string) override;

		private:
			bool isReady();
			void putChar(char ch);
		};
	}
}
