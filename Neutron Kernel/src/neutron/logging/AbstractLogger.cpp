#include <neutron.h>
#include <klibc/kprintf.h>
#include "AbstractLogger.h"

namespace Neutron {
	namespace Logging {
		void AbstractLogger::setLogLevel(LogLevel level) {
			_level = level;
		}

		LogLevel AbstractLogger::getLogLevel() {
			return _level;
		}

		void AbstractLogger::log(LogLevel level, const char* format, ...) {
			if (level >= _level) {
				va_list args;
				va_start(args, format);
				kvsprintf(reinterpret_cast<char*>(&_logBuffer), format, args);
				va_end(args);

				internalLog(reinterpret_cast<char*>(&_logBuffer));
			}
		}
	}
}
