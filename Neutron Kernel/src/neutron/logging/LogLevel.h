#pragma once
#include <neutron.h>

namespace Neutron {
	namespace Logging {
		enum LogLevel : uint8_t {
			InterruptTrace,
			Trace,
			Debug,
			Info,
			Warn,
			Error,
			Fail,
			Fatal,
			Print,
			Off
		};
	}
}
