#pragma once
#include <neutron.h>
#include "LogLevel.h"

#if (defined(DEBUG) && !defined(NO_DEBUG_LOG))
#define LOG_INTERRUPT_TRACE(...) Neutron::NeutronKernel::getInstance()->getLogger()->log(Neutron::Logging::LogLevel::InterruptTrace, __VA_ARGS__)
#define LOG_TRACE(...) Neutron::NeutronKernel::getInstance()->getLogger()->log(Neutron::Logging::LogLevel::Trace, __VA_ARGS__)
#define LOG_DEBUG(...) Neutron::NeutronKernel::getInstance()->getLogger()->log(Neutron::Logging::LogLevel::Debug, __VA_ARGS__)
#else
#define LOG_INTERRUPT_TRACE(...)
#define LOG_TRACE(...)
#define LOG_DEBUG(...)
#endif

#define LOG_INFO(...) Neutron::NeutronKernel::getInstance()->getLogger()->log(Neutron::Logging::LogLevel::Info, __VA_ARGS__)
#define LOG_WARN(...) Neutron::NeutronKernel::getInstance()->getLogger()->log(Neutron::Logging::LogLevel::Warn, __VA_ARGS__)
#define LOG_ERROR(...) Neutron::NeutronKernel::getInstance()->getLogger()->log(Neutron::Logging::LogLevel::Error, __VA_ARGS__)
#define LOG_FAIL(...) Neutron::NeutronKernel::getInstance()->getLogger()->log(Neutron::Logging::LogLevel::Fail, __VA_ARGS__)
#define LOG_FATAL(...) Neutron::NeutronKernel::getInstance()->getLogger()->log(Neutron::Logging::LogLevel::Fatal, __VA_ARGS__)
#define LOG_PRINT(...) Neutron::NeutronKernel::getInstance()->getLogger()->log(Neutron::Logging::LogLevel::Print, __VA_ARGS__)

namespace Neutron {
	namespace Logging {
		class ILogger {
		public:
			virtual ~ILogger() { }

			virtual void setLogLevel(LogLevel level) = 0;
			virtual LogLevel getLogLevel() = 0;

			virtual void log(LogLevel level, const char* format, ...) = 0;
		};
	}
}
