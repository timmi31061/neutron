#pragma once
#include <neutron.h>
#include "ILogger.h"

namespace Neutron {
	namespace Logging {
		class AbstractLogger : public ILogger {
		public:
			virtual void setLogLevel(LogLevel level) override;
			virtual LogLevel getLogLevel() override;
			virtual void log(LogLevel level, const char* format, ...) override;

		protected:
			virtual void internalLog(const char* string) = 0;

		private:
			LogLevel _level;
			char _logBuffer[1024];
		};
	}
}
