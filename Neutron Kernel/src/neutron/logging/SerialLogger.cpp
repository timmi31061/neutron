#include <neutron.h>
#include<neutron/arch/Architecture.h>
#include "SerialLogger.h"

#define PORT 0x3f8 // COM1

namespace Neutron {
	namespace Logging {
		SerialLogger::SerialLogger() {
			Arch::Architecture::currentArch()->outb(PORT + 1, 0x00); // Disable all interrupts
			Arch::Architecture::currentArch()->outb(PORT + 3, 0x80); // Enable DLAB (set baud rate divisor)
			Arch::Architecture::currentArch()->outb(PORT + 0, 0x01); // Set divisor to 1 (lo byte) 115200 baud
			Arch::Architecture::currentArch()->outb(PORT + 1, 0x00); //                  (hi byte)
			Arch::Architecture::currentArch()->outb(PORT + 3, 0x03); // 8 bits, no parity, one stop bit
			Arch::Architecture::currentArch()->outb(PORT + 2, 0xC7); // Enable FIFO, clear them, with 14-byte threshold
			Arch::Architecture::currentArch()->outb(PORT + 4, 0x0B); // IRQs enabled, RTS/DSR set

			internalLog("");
			internalLog("======== LOG STARTED ========");
			internalLog("");
		}

		void SerialLogger::internalLog(const char* string) {
			while (*string) {
				putChar(*string++);
			}

			putChar('\n');
		}

		bool SerialLogger::isReady() {
			return (Arch::Architecture::currentArch()->inb(PORT + 5) & 0x20) != 0;
		}

		void SerialLogger::putChar(char ch) {
			if (ch == '\n') {
				putChar('\r');
			}

			while (isReady() == 0);

			Arch::Architecture::currentArch()->outb(PORT, ch);
		}
	}
}
