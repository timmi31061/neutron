#pragma once
#include <neutron.h>
#include "AbstractLogger.h"

namespace Neutron {
	namespace Logging {
		class NullLogger final : public AbstractLogger {
		public:
			virtual void internalLog(const char * string) override;
		};
	}
}
