#include <neutron.h>
#include <neutron/NeutronKernel.h>
#include <neutron/logging/ILogger.h>
#include "ManualResetEvent.h"

namespace Neutron {
	namespace Multitasking {
		ManualResetEvent::ManualResetEvent() {
			_state = false;
		}

		ManualResetEvent::ManualResetEvent(bool initialState) {
			_state = initialState;
		}

		void ManualResetEvent::set() {
			LOG_TRACE("ManualResetEvent::set");
			_state = true;
		}

		void ManualResetEvent::reset() {
			LOG_TRACE("ManualResetEvent::reset");
			_state = false;
		}

		void ManualResetEvent::waitOne() {
			LOG_TRACE("ManualResetEvent::waitOne");
			while (!_state);
		}
	}
}
