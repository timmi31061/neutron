#pragma once
#include <neutron.h>

namespace Neutron {
	namespace Multitasking {
		class NEUTRON_MODULE_API ManualResetEvent {
		public:
			ManualResetEvent();
			ManualResetEvent(bool initialState);

			void set();
			void reset();
			void waitOne();

		protected:
			volatile bool _state;
		};
	}
}
