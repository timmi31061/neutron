#include <neutron.h>
#include <neutron/arch/Architecture.h>
#include "ThreadLock.h"

namespace Neutron {
	namespace Multitasking {
		ThreadLock::ThreadLock() {
		}

		ThreadLock::ThreadLock(ThreadLockState initialState) {
			_state = initialState;
		}

		bool ThreadLock::isLocked() {
			return _state == ThreadLockState::Locked;
		}

		void ThreadLock::release() {
			_state = ThreadLockState::Available;
		}

		bool ThreadLock::lock() {
			ThreadLockState lockState = ThreadLockState::Locked;
			Arch::Architecture::currentArch()->atomicXchg(reinterpret_cast<uint32_t*>(&_state), reinterpret_cast<uint32_t*>(&lockState));

			return lockState == ThreadLockState::Available;
		}

		void ThreadLock::lockWait() {
			while (lock() == false);
		}
	}
}
