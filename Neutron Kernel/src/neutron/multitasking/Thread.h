#pragma once
#include <neutron.h>
#include <neutron/arch/CpuState.h>
#include "ThreadLock.h"

namespace Neutron {
	namespace Multitasking {
		enum class ThreadState : uint8_t {
			Invalid,
			Blocked,
			Running,
			Aborted
		};

		struct NEUTRON_MODULE_API Thread {
			Thread();

			uint32_t threadID;
			ThreadState taskState;
			ThreadLock* lock;

			uint8_t slotsPerRound;
			uint8_t usedSlots;

			uintptr_t stack;
			Neutron::Arch::CpuState* cpuState;
		};
	}
}
