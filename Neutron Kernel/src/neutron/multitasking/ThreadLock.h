#pragma once
#include <neutron.h>

namespace Neutron {
	namespace Multitasking {
		enum class ThreadLockState : uint32_t {
			Available = 0,
			Locked = 1
		};

		class NEUTRON_MODULE_API ThreadLock {
		public:
			ThreadLock();
			ThreadLock(ThreadLockState initialState);

			bool isLocked();
			void release();
			bool lock();
			void lockWait();

		protected:
			volatile ThreadLockState _state = ThreadLockState::Available;
		};
	}
}
