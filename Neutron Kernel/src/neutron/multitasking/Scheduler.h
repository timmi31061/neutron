#pragma once
#include <neutron.h>
#include <kstl/list.h>
#include <neutron/arch/CpuState.h>
#include <neutron/arch/VirtualMemoryContext.h>
#include "Thread.h"

namespace Neutron {
	namespace Multitasking {
		class NEUTRON_MODULE_API Scheduler {
		public:
			Scheduler();
			Thread* createThread(uintptr_t entry, Neutron::Arch::VirtualMemoryContext* context, bool kernelMode = false);
			Neutron::Arch::CpuState* schedule();
			Thread* currentThread();

		private:
			Thread* nextThread();

			ThreadLock* _lockState;
			uint32_t _currentThreadIndex = 0;
			uint32_t _maxThreadID = 0;
			kstl::ArrayList<Thread*> _threads;
			//kstl::ArrayList<uint32_t> _inBetweenThreadIDs;

			bool _isIdle = true;
			Thread* _idleThread;
		};
	}
}
