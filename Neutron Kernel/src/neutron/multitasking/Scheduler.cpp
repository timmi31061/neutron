#include <neutron.h>
#include <neutron/arch/Architecture.h>
#include "Scheduler.h"
#include <klibc/kprintf.h>

#include <neutron/NeutronKernel.h>
#include <neutron/arch/x86/CpuStateX86.h>
#include <neutron/logging/ILogger.h>

#define DEFAULT_STACK_SIZE 8192

#define NEXT_THREAD_ID (_currentThreadIndex = ++_currentThreadIndex % _threads.length())
#define NEXT_THREAD (_threads[NEXT_THREAD_ID])
#define CURRENT_THREAD (_threads[_currentThreadIndex])

using namespace Neutron::Arch;
using namespace Neutron::Arch::X86;

namespace Neutron {
	namespace Multitasking {
		static void idleThread() {
			while (1) {
				Architecture::currentArch()->halt();
			}
		}

		Scheduler::Scheduler() {
			// Setup idle thread
			_idleThread = new Thread();
			_idleThread->threadID = 0;
			_idleThread->taskState = ThreadState::Running;
			_idleThread->stack = new uint8_t[DEFAULT_STACK_SIZE + Architecture::currentArch()->cpuStateSize()] + DEFAULT_STACK_SIZE;
			_idleThread->slotsPerRound = 1;
			_idleThread->usedSlots = 0;
			_idleThread->cpuState = reinterpret_cast<CpuState*>(_idleThread->stack);
			Architecture::currentArch()->initializeThreadCpuState(_idleThread->cpuState, &idleThread, _idleThread->stack, Architecture::currentArch()->kernelContext(), true);

			// Initialize lock state
			_lockState = new ThreadLock();
		}

		Thread* Scheduler::createThread(uintptr_t entry, VirtualMemoryContext* context, bool kernelMode) {
			// Allocate memory before locking. This can be slow.
			Thread* thread = new Thread();
			uint8_t* stack = new uint8_t[DEFAULT_STACK_SIZE] + DEFAULT_STACK_SIZE;

			// Lock, because we're modifying stuff. If this
			// is interrupted, bad things will happen.
			_lockState->lockWait();

			// Prepare thread
			thread->threadID = ++_maxThreadID;
			thread->taskState = ThreadState::Running;
			thread->stack = stack;
			thread->slotsPerRound = 1;
			thread->usedSlots = 0;
			thread->cpuState = reinterpret_cast<CpuState*>(stack - Architecture::currentArch()->cpuStateSize());
			Architecture::currentArch()->initializeThreadCpuState(thread->cpuState, entry, thread->stack, context, kernelMode);

			//kprintf("task (%X): %X\n", thread->threadID, thread->stack);

			_threads.add(thread);
			_lockState->release();

			return thread;
		}

		CpuState* Scheduler::schedule() {
			if (_threads.length() > 0) {
				bool switchToNextThread = true;

				if (_isIdle == false) {
					// Increment slot counter
					_threads[_currentThreadIndex]->usedSlots++;

					// If the thread consumed its time, move to the next one
					if (_threads[_currentThreadIndex]->usedSlots >= _threads[_currentThreadIndex]->slotsPerRound) {
						// Zero slot counter
						_threads[_currentThreadIndex]->usedSlots = 0;

						// The CpuState is saved on the stack of the running thread. That is exactly where we want it to be.
						_threads[_currentThreadIndex]->cpuState = Architecture::currentArch()->cpuState();

						// Switch thread
						switchToNextThread = true;
					}
					else {
						switchToNextThread = false;
					}
				}

				if (switchToNextThread) {
					return nextThread()->cpuState;
				}
			}

			return Architecture::currentArch()->cpuState();
		}

		Thread* Scheduler::currentThread() {
			return _threads[_currentThreadIndex];
		}

		Thread* Scheduler::nextThread() {
			if (_threads.length() > 0) {
				bool newThreadFound = false;
				uint32_t i = _currentThreadIndex;
				do {
					i = ++i % _threads.length();

					// Thread is aborted => delete it
					if (_threads[i]->taskState == ThreadState::Aborted) {
						// ToDo: Delete stack
						_threads.removeAt(i);
						i--;
						continue;
					}

					// Thread is blocked, but the reason of block is gone
					if (_threads[i]->taskState == ThreadState::Blocked && _threads[i]->lock && _threads[i]->lock->isLocked() == false){
						_threads[i]->taskState = ThreadState::Running;
					}

					// Thread is running
					if (_threads[i]->taskState == ThreadState::Running) {
						newThreadFound = true;
						break;
					}
				} while (i != _currentThreadIndex);

				if (newThreadFound) {
					_currentThreadIndex = i;
					_isIdle = false;
					return currentThread();
				}
			}

			_isIdle = true;
			return _idleThread;
		}
	}
}
