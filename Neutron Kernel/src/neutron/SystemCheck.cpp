#include <neutron.h>
#include <neutron/NeutronKernel.h>
#include <neutron/arch/Architecture.h>
#include <neutron/logging/ILogger.h>
#include "SystemCheck.h"

using namespace Neutron::Arch;

#define FAIL_IF(condition) if(condition) NeutronKernel::getInstance()->panic(0, "Neutron self test failed. Line %s", __LINE_STR__)

namespace Neutron {
	void SystemCheck::invoke() {
		LOG_DEBUG("checkPMM");
		SystemCheck::checkPMM();

		LOG_DEBUG("checkHeap");
		SystemCheck::checkHeap();

		LOG_DEBUG("checAtomicXchg");
		SystemCheck::checAtomicXchg();
	}

	void SystemCheck::checkPMM() {
		PhysicalMemoryManager* pmm = Architecture::currentArch()->pmm();

		uint32_t ptr1 = reinterpret_cast<uint32_t>(pmm->alloc());
		uint32_t ptr2 = reinterpret_cast<uint32_t>(pmm->alloc());
		uint32_t ptr3 = reinterpret_cast<uint32_t>(pmm->alloc());
		FAIL_IF(ptr1 == 0 || ptr2 == 0 || ptr3 == 0);
		FAIL_IF(ptr1 > ptr2 || ptr2 > ptr3);

		pmm->free(reinterpret_cast<uintptr_t>(ptr2));
		pmm->free(reinterpret_cast<uintptr_t>(ptr3));
		ptr3 = reinterpret_cast<uint32_t>(pmm->alloc());
		FAIL_IF(ptr2 != ptr3);

		pmm->free(reinterpret_cast<uintptr_t>(ptr1));
		pmm->free(reinterpret_cast<uintptr_t>(ptr3));
	}

	void SystemCheck::checkHeap() {
		HeapManager* heap = Architecture::currentArch()->heap();

		/*_asm {
			pushfd;
			pop eax;
			or eax, (1 << 8);
			push eax;
			popfd;
		}*/

		LOG_TRACE("    checkHeap 1 %X", heap);
		uint32_t ptr1 = reinterpret_cast<uint32_t>(heap->alloc(16));
		LOG_TRACE("    checkHeap 2");
		uint32_t ptr2 = reinterpret_cast<uint32_t>(heap->alloc(16));
		LOG_TRACE("    checkHeap 3");
		uint32_t ptr3 = reinterpret_cast<uint32_t>(heap->alloc(16));
		LOG_TRACE("    checkHeap 4");
		FAIL_IF(ptr1 == 0 || ptr2 == 0 || ptr3 == 0);
		LOG_TRACE("    checkHeap 5");
		FAIL_IF(ptr1 > ptr2 || ptr2 > ptr3);
		LOG_TRACE("    checkHeap 6");

		heap->free(reinterpret_cast<uintptr_t>(ptr2));
		LOG_TRACE("    checkHeap 7");
		heap->free(reinterpret_cast<uintptr_t>(ptr3));
		LOG_TRACE("    checkHeap 8");
		ptr3 = reinterpret_cast<uint32_t>(heap->alloc(16));
		LOG_TRACE("    checkHeap 9");
		FAIL_IF(ptr2 != ptr3);
		LOG_TRACE("    checkHeap 10");

		heap->free(reinterpret_cast<uintptr_t>(ptr1));
		LOG_TRACE("    checkHeap 11");
		heap->free(reinterpret_cast<uintptr_t>(ptr3));
		LOG_TRACE("    checkHeap 12");
	}

	void SystemCheck::checAtomicXchg() {
		uint32_t i1 = 0;
		uint32_t i2 = 1;

		FAIL_IF(i1 != 0 || i2 != 1);
		Architecture::currentArch()->atomicXchg(&i1, &i2);
		FAIL_IF(i1 != 1 || i2 != 0);
	}
}
