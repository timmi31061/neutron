#pragma once
#include <neutron.h>

namespace Neutron {
	class SystemCheck {
	public:
		static void invoke();

	private:
		static void checkPMM();
		static void checkHeap();
		static void checAtomicXchg();
	};
}
