#pragma once

#include <neutron/arch/Architecture.h>
#include <klibc/kprintf.h>

#define kout (*Neutron::Arch::Architecture::currentArch()->console())
#define endl "\r\n"
