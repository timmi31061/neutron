#pragma once

/**
 * Defines
 */
// Kernel-stack base
#define KERNEL_STACK 0x27FFFF
#define STACK_SIZE 0x80000

// Base address from advanced linker option
#define LOAD_BASE 0x400000

// We're building for x86
#define ARCH_X86

// See memory map
#define LO_MEM 0x300000
#define ADDITIONAL_MEM 0x100000

// Export or import neutron module API
#ifdef NEUTRON_MODULE_EXPORT
#define NEUTRON_MODULE_API __declspec(dllexport)
#else
#define NEUTRON_MODULE_API __declspec(dllimport)
#endif

extern void* endOfImage;

/**
 * Typedefs
 */
typedef signed char			int8_t;
typedef signed short		int16_t;
typedef signed long int		int32_t;
typedef signed long long	int64_t;

typedef unsigned char		uint8_t;
typedef unsigned short		uint16_t;
typedef unsigned long int	uint32_t;
typedef unsigned long long	uint64_t;

typedef void* uintptr_t;

//typedef uint32_t size_t;

#define NULL_PTR ((uintptr_t)0)

/**
 * Variable argument lists
 */
#define _ADDRESSOF(v)   ( &reinterpret_cast<const char &>(v) )
#define _INTSIZEOF(n)   ( (sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1) )

typedef char*  va_list;
#define va_start(ap,v)  ( ap = (va_list)_ADDRESSOF(v) + _INTSIZEOF(v) )
#define va_arg(ap,t)    ( *(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)) )
#define va_end(ap)      ( ap = (va_list)0 )

/**
 * Useful macros
 */
#define CONCAT(a,b) a##b
#define MIN(a,b) (a <= b ? a : b)
#define MAX(a,b) (a >= b ? a : b)
#define CEILDIV(a,b) (((a - 1) / b) + 1)

#define DO_STRINGIZE(v) #v
#define STRINGIZE(v) DO_STRINGIZE(v)
#define __LINE_STR__ STRINGIZE(__LINE__)

#define KSTART_PAGE (LOAD_BASE / Architecture::currentArch()->pageSize())
#define KEND_PAGE CEILDIV(reinterpret_cast<uint32_t>(&endOfImage) + ADDITIONAL_MEM, Architecture::currentArch()->pageSize())

/**
 * Deactivated Warnings (in compiler settings)
 */
// 4100: unreferenced formal parameter
// 4189: local variable is initialized but not referenced
// 4251: class 'type' needs to have dll-interface to be used by clients of class 'type2'
// 4365: signed/unsigned mismatch
// 4464: relative include path contains '..'
// 4514: unreferenced inline function has been removed
// 4571: catch(...) semantics changed since Visual C++ 7.1; structured exceptions (SEH) are no longer caught
// 4634: XML document comment target: cannot be applied
// 4710: function not inlined
// 4711: function selected for inline expansion
// 4714: 'function' marked as __forceinline not inlined
// 
