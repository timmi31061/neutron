#include <neutron.h>
#include <kiostream.h>
#include <crt/crt.h>

#include <neutron/arch/x86/ArchitectureX86.h>
#include <neutron/arch/InitializationInfo.h>
#include <neutron/logging/SerialLogger.h>
#include <neutron/NeutronKernel.h>
#include <neutron/SystemCheck.h>

#include "neutron\modules\pe.h"

#include "multiboot.h"

using namespace Neutron::Arch;
using namespace Neutron::Arch::X86;
using namespace Neutron::Logging;
using namespace Neutron;

ArchitectureX86 arch;
NeutronKernel kernel;

extern "C" void kmain(uint32_t magic, mb_struct* _mbinfo)
{
	// Invoke constructors
	_crt_invokeConstructors();

	// Bind HAL
	Architecture::setArch(&arch);

	// Check magic number
	if (magic != MB_MAGIC) {
		kout << ConsoleColor::White << BackgroundConsoleColor(ConsoleColor::Red);
		kout.clear();

		// Print message centered
		kout.row = static_cast<uint8_t>(kout.height() / 2);
		kout << "\033cNeutron startup failed: Not loaded by a multiboot-compliant bootloader!";

		// Halt system
		Architecture::currentArch()->halt(true);
	}

	// Create debug logger
	SerialLogger logger;
	logger.setLogLevel(LogLevel::Trace);
	NeutronKernel::getInstance()->setLogger(&logger);

	// Compiler doesn't recompile this unless this file changed.
	//LOG_DEBUG("-- Booting Neutron - Debug Build compiled at " __DATE__ " " __TIME__ " --");

	// Initialize Hardware
	InitializationInfo info = InitializationInfo(mbinfo);
	Architecture::enable(&info);

	LOG_INFO("Hardware initialized.");

	// Print message (users are usually happy to see something blinking)
	kout << "Neutron is booting...\n";

	// Perform basic system checks
	LOG_DEBUG("Performing basic system checks.");
	SystemCheck::invoke();

	// Boot kernel
	LOG_DEBUG("Initializing kernel.");
	NeutronKernel::getInstance()->init();

	LOG_INFO("Loading modules.");
	kout << "Loading modules...\n";
	PE::PeImage* kernelImage = new PE::PeImage(LOAD_BASE, true, LOAD_BASE);
	kernelImage->scanExports();

	kstl::LinearMap<char*, kstl::LinearMap<char*, uint32_t>*> libraries;
	libraries.add("neutron.sys", &kernelImage->_exportedFunctions);

	for (uint32_t i = 0; i < mbinfo->moduleCount; i++) {
		mb_module* mod = &mbinfo->modules[i];
		kout << "  " << mod->name << "\n";
		PE::PeImage* module = new PE::PeImage(mod->startAddress);
		module->linkImports(&libraries);
		void(*mmain)() = (void(*)())module->entryPoint();

		NeutronKernel::getInstance()->scheduler()->createThread(mmain, Architecture::currentArch()->kernelContext(), true);
	}

	LOG_INFO("Entering multitasking mode.");
	NeutronKernel::getInstance()->enableMultitasking();

	// Halt system softly (leaving interrupts enabled)
	Architecture::currentArch()->halt();

	// Dummy try-catch to mark the end of the callstack.
	try {} catch(...) {}
}
