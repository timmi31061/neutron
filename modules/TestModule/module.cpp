#pragma comment(lib, "neutron.lib")

#include <klibc/kprintf.h>
#include <neutron/arch/Architecture.h>
#include <neutron/NeutronKernel.h>

extern "C" int __declspec(dllexport) getModID() {
	return 42;
}

extern "C" int __declspec(dllexport) getModFlags() {
	return 42;
}

void mmain() {
	char* video = (char*)0xB8000;
	char* hello = "Hello World!";

	kprintf("======== HELLO WORLD! ========\n");
	Neutron::Arch::Architecture::currentArch()->console()->operator<<("This is through HAL!\n");

	video += 320 - 24;

	while (*hello) {
		*(video++) = *(hello++);
		*(video++) = 0x17;

	}

	kprintf("Neutron::NeutronKernel::getInstance()\n");
	auto t = Neutron::NeutronKernel::getInstance()->scheduler()->currentThread();
	kprintf("TID: %d\n", t->threadID);
	t->taskState = Neutron::Multitasking::ThreadState::Aborted;

	// ToDo: Returing from threads doesn't work.
	while (1) {
		kprintf("======== Still running ========\n");
		for (int i = 0; i < 80000; i++) {
			_asm pause;
		}
	}
	//Neutron::NeutronKernel::getInstance()->panic(0xDEADBEEF, "Module loaded!");
	
}