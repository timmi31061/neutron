@ECHO OFF
REM ##############################################################################
REM (c) Eric Lassauge - April 2013
REM <lassauge {AT} users {DOT} sourceforge {DOT} net >
REM
REM ##############################################################################
REM    This program is free software: you can redistribute it and/or modify
REM    it under the terms of the GNU General Public License as published by
REM    the Free Software Foundation, either version 3 of the License, or
REM    (at your option) any later version.
REM
REM    This program is distributed in the hope that it will be useful,
REM    but WITHOUT ANY WARRANTY; without even the implied warranty of
REM    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM    GNU General Public License for more details.
REM
REM    You should have received a copy of the GNU General Public License
REM    along with this program.  If not, see <http://www.gnu.org/licenses/>
REM ##############################################################################
REM Exemple BAT script for starting qemu-system-ppc.exe on windows host with a
REM virtex-ml507 linux kernel
REM to test QEMU (See http://wiki.qemu.org/Testing)

REM Start qemu on windows.

REM SDL_VIDEODRIVER=directx is faster than windib. But keyboard may not work well.
SET SDL_VIDEODRIVER=directx

REM QEMU_AUDIO_DRV=dsound or fmod or sdl or none can be used. See qemu -audio-help.
REM dsound seems to be broken now (at least in my build)
SET QEMU_AUDIO_DRV=sdl

REM SDL_AUDIODRIVER=waveout or dsound can be used. Only if QEMU_AUDIO_DRV=sdl.
SET SDL_AUDIODRIVER=dsound

REM QEMU_AUDIO_LOG_TO_MONITOR=1 displays log messages in QEMU monitor.
SET QEMU_AUDIO_LOG_TO_MONITOR=1

REM ################################################
REM # Boot kernel: add ppc.dtb?
REM ################################################
START qemu-system-ppcw.exe -L Bios -m 256M -k fr -M virtex-ml507 -vnc localhost:0 ^
-name PPC-virtex-ml507 -kernel test-ppc/vmlinux -dtb test-ppc/ppc.dtb ^
-append "rdinit=/boot.sh console=ttyS0"